package nmg.online.ui;

import java.util.List;

import nmg.demo.activity.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MyListAdapter extends BaseAdapter {
	private List<String> mList;
	private LayoutInflater inflater;

	public MyListAdapter(Context mContext, List<String> mList) {
		this.inflater = LayoutInflater.from(mContext);
		this.mList = mList;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder = null;
		if (convertView == null) {
			holder = new Holder();
			convertView = inflater.inflate(R.layout.demo_item, null);
			holder.mTextView = (TextView) convertView
					.findViewById(R.id.text_item);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		holder.mTextView.setText(mList.get(position));
		System.out.println("mList.get(position)==" + mList.get(position));
		return convertView;
	}

	class Holder {
		private TextView mTextView;
	}
}
