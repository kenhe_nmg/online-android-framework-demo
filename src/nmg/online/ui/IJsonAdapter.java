package nmg.online.ui;

import java.io.Serializable;

import android.os.AsyncTask;
/**
 * IJsonAdapter
 * Json 获取接口
 * @author 		Ken
 * @created		2011-9-12
 * @since		2011-9-27
 */
public abstract interface IJsonAdapter {
	//取消获取时调用
	public abstract void onCancelled();

	//获取完成时调用
	public abstract void onFinished(AsyncTask<?, ?, ?> source, Serializable datas);

	//开始获取时被调用
	public abstract void onBegin();
}
