package nmg.online.model;

public class Student {
	private int id;
	private String name;
	private int age;
	private double heigh;
	
	public Student() {
	}
	
	public Student(int id, String name, int age, double heigh) {
		this.id = id;
		this.name = name;
		this.age = age;
		this.heigh = heigh;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public double getHeigh() {
		return heigh;
	}
	public void setHeigh(double heigh) {
		this.heigh = heigh;
	}
	
	
}
