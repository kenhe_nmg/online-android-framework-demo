package nmg.online.model;

import java.io.Serializable;
import java.util.List;

public class NewsDataBody implements Serializable{

	public int success;
	public String msg;
	private static final long serialVersionUID = 1L;
	public List<NewsData> data;
}

class CouponData {
	public String coupon_id;
	public String download_token;
	public String state;
	public String expired;
	public String redemption_no;
	public String[] pic;
}
