package nmg.online.model;

import java.util.List;


public class NewsData{

    public int news_id;
    public String news_title;
    public String news_content;
    public String news_categoryname;
    public String news_date;
    public String news_date2;
    public String news_videourl;
    public String news_thumb;
    public String news_url;
    public List<String[]> newsphoto_list;
    public int type;
    public int status;
    public String shop_latlng;
    
    public String is_coupon;
    public CouponData coupon;
    public String coupon_state;
    
}
