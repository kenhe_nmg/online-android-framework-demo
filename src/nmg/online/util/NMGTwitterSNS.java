package nmg.online.util;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

public class NMGTwitterSNS implements NMGSns {

	private Context context;
	private Activity activity;
	public static Twitter twitter;
	public static boolean isTwitterLogin;
	public static RequestToken requestToken;
	public static AccessToken accessToken;

	public NMGTwitterSNS(Context context, Activity activity) {
		this.context = context;
		this.activity = activity;
	}

	/**
	 * 判断Twitter是否登录
	 */
	@Override
	public void isLogin() {
		// TODO Auto-generated method stub
		String twitter_accessToken = (String)NMGConfigHelper.getSharePref(context,NMGPublicData.SHARED_PRE_SAVE_FILE_NAME,
				NMGPublicData.PREF_KEY_TOKEN, NMGConfigHelper.DATA_STRING);
		String twitter_accessSecret = (String)NMGConfigHelper.getSharePref(context,NMGPublicData.SHARED_PRE_SAVE_FILE_NAME,
				NMGPublicData.PREF_KEY_SECRET, NMGConfigHelper.DATA_STRING);
		twitter = new TwitterFactory().getInstance();
		twitter.setOAuthConsumer(NMGPublicData.CONSUMER_KEY,NMGPublicData.CONSUMER_SECRET);
		if(twitter_accessToken != null&&!twitter_accessToken.equals("")){
			accessToken = new AccessToken(twitter_accessToken,twitter_accessSecret);
			twitter.setOAuthAccessToken(accessToken);
			isTwitterLogin = true;
		}
		
	}

	/**
	 * Twitter登录
	 */
	@Override
	public void snsLogin() {
		// TODO Auto-generated method stub
		if (!isTwitterLogin) {
			new TwitterTask().execute();
		} else {
			Toast.makeText(context, "Twitter已登录", Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * 退出Twitte
	 */
	@Override
	public void snsLogout() {
		// TODO Auto-generated method stub
		if(isTwitterLogin){
			NMGConfigHelper.setSharePref(context,NMGPublicData.SHARED_PRE_SAVE_FILE_NAME,NMGPublicData.PREF_KEY_TOKEN, "");
			NMGConfigHelper.setSharePref(context,NMGPublicData.SHARED_PRE_SAVE_FILE_NAME,NMGPublicData.PREF_KEY_SECRET, "");
			twitter = null;
			requestToken = null;
//			CookieManager cookieManager = CookieManager.getInstance();
//			cookieManager.removeAllCookie();
			isTwitterLogin = false;
			Toast.makeText(context, "Twitter退出成功", Toast.LENGTH_SHORT).show();
		}else{
			Toast.makeText(context, "Twitter已退出", Toast.LENGTH_SHORT).show();
		}
		
	}

	@Override
	public void snsShare() {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 分享Twitter内容
	 * @param content
	 */
	public void snsShare(String content){
		if(isTwitterLogin){
			new Share2Twitter(content).execute();
		}else{
			Toast.makeText(context, "please 登录Twitter", Toast.LENGTH_SHORT).show();
		}
		
	}

	// 登录到 twitter
	private class TwitterTask extends AsyncTask<Void, Void, Void> {
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(context);
			progressDialog.setMessage("Please Wait...");
			progressDialog.setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					cancel(false);
				}
			});
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... v) {
			connectTwitter();
			return (Void) null;
		}

		@Override
		protected void onProgressUpdate(Void... v) {

		}

		@Override
		protected void onPostExecute(Void v) {
			progressDialog.dismiss();
		}
	}

	private void connectTwitter() {
		ConfigurationBuilder confbuilder = new ConfigurationBuilder();
		Configuration conf = confbuilder.setOAuthConsumerKey(NMGPublicData.CONSUMER_KEY).setOAuthConsumerSecret(NMGPublicData.CONSUMER_SECRET).build();
		twitter = new TwitterFactory(conf).getInstance();
		twitter.setOAuthAccessToken(null);

		try {
			requestToken = twitter.getOAuthRequestToken(NMGPublicData.CALLBACK_URL);
			Intent intent = new Intent(activity, TwitterLoginBrowser.class);
			intent.putExtra(NMGPublicData.IEXTRA_AUTH_URL,requestToken.getAuthorizationURL());
			activity.startActivity(intent);
		} catch (TwitterException e) {
			e.printStackTrace();
			Toast.makeText(context, "Errror : " + e.getStatusCode(),Toast.LENGTH_LONG).show();
		}
	}

	public class Share2Twitter extends AsyncTask<Void, Void, Boolean> {
		private ProgressDialog progressDialog;
		private String sendMessage;
		public Share2Twitter(String sendMessage) {
			super();
			this.sendMessage = sendMessage;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(context);
			progressDialog.setMessage("Please Wait ...");
			progressDialog.setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					cancel(false);
				}
			});
			progressDialog.show();
		}

		@Override
		protected Boolean doInBackground(Void... v) {
			twitter4j.Status status = null;
			try {
				// twitter.sendDirectMessage(1, str);
				status = twitter.updateStatus(sendMessage);
			} catch (TwitterException e) {

				e.printStackTrace();
			}
			if (null == status || "".equals(status.getId())) {
				return false;
			} else {
				return true;
			}
		}

		@Override
		protected void onProgressUpdate(Void... v) {
			// TODO show progress
		}

		@Override
		protected void onPostExecute(Boolean v) {
			progressDialog.dismiss();
			if (v) {
				Toast.makeText(context, "分享到twitter成功！ ", Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(context, "分享到twitter失敗，請檢查你的網絡或請勿重複發送！ ",Toast.LENGTH_LONG).show();
			}
		}
	}

}
