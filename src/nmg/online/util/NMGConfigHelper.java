package nmg.online.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class NMGConfigHelper {
	private static final String TAG = "NMGConfigHelper" ;
	
	// 保存模式：1:String ,2:int ,3:long,  4:boolean, 5:Float
	public static final int DATA_STRING = 1;
	public static final int DATA_INT = 2;
	public static final int DATA_LONG = 3;	
	public static final int DATA_BOOLEAN = 4;
	public static final int DATA_FLOAT = 5;
	
	/**
	 * 把配置的键值对 写入配置文件
	 * @param context
	 * @param fileName
	 * @param configKey
	 * @param configValue
	 * @return
	 */
	public static boolean setSharePref(Context context, String fileName, String configKey ,  Object configValue )
	{
		SharedPreferences preferences = context.getSharedPreferences(fileName, Context.MODE_WORLD_READABLE);
		Editor editor = preferences.edit();					
		
		Log.i(TAG,"configValue.getClass() = " + configValue.getClass());
		
		if(configValue.getClass() == String.class) // String类型
		{
			Log.i(TAG,"String.class");
			String str = (String)configValue;
			editor.putString(configKey, str); 
			
			
		}else if(configValue.getClass() == Integer.class)// Integer类型
		{
			Integer i = (Integer)configValue;
			editor.putInt(configKey, i);
			
		}else if(configValue.getClass() == Boolean.class)// Boolean类型
		{
			Boolean b = (Boolean)configValue;
			editor.putBoolean(configKey, b);
			
		}else if(configValue.getClass() == Long.class)// Long类型
		{
			Long l = (Long)configValue;
			editor.putLong(configKey, l);
			
		}else if(configValue.getClass() == Float.class)// Float类型
		{
			Float f = (Float)configValue;
			editor.putFloat(configKey, f);
			
		}else
		{
			return false;
		}
		editor.commit();
		return true;
				
	}
	/**
	 * 读取配置文件的值，返回是object类型: 1:String ,2:int ,3:long,  4:boolean, 5:Float
	 * @param context
	 * @param fileName
	 * @param configKey
	 * @param dataType
	 * @return
	 */
	public static Object getSharePref(Context context, String fileName, String configKey , int dataType)
	{
		SharedPreferences preferences = context.getSharedPreferences(fileName, Context.MODE_WORLD_READABLE);
		switch (dataType) {
		case DATA_STRING :
			return preferences.getString(configKey, "");
		case DATA_INT :
			return preferences.getInt(configKey, 0);
		case DATA_LONG :
			return preferences.getLong(configKey, 0L);
		case DATA_BOOLEAN :
			return preferences.getBoolean(configKey, false);
		case DATA_FLOAT:
			return preferences.getFloat(configKey,  0.0f);
			

		default:
			return null ;
		}	
		
	}
	

}
