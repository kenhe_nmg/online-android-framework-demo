package nmg.online.util;

import java.io.IOException;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;
import com.weibo.net.AccessToken;
import com.weibo.net.AsyncWeiboRunner;
import com.weibo.net.AsyncWeiboRunner.RequestListener;
import com.weibo.net.Oauth2AccessTokenHeader;
import com.weibo.net.Utility;
import com.weibo.net.Weibo;
import com.weibo.net.WeiboDialogListener;
import com.weibo.net.WeiboException;
import com.weibo.net.WeiboParameters;


public class NMGSinaSNS implements NMGSns, RequestListener{

	private String sinaConsumerKey = "489697180";
	private String sinaConsumerSecret = "f1609d3488192d8b1d9746759a50bd86";
	private String URL = "http://open.weibo.com/apps/489697180/info/advanced";
	private Weibo mWeibo = Weibo.getInstance();
	private static boolean isSinalogin = false;
	private AccessToken accessToken;
	private Context context;
	private Activity activity;
	
	public NMGSinaSNS(Context context,Activity activity){
		this.context = context;
		this.activity = activity;
	}
	/**
	 * 登录新浪微博
	 */
	@Override
	public void snsLogin() {
		// TODO Auto-generated method stub
		if (!isSinalogin) {
			mWeibo.setupConsumerConfig(sinaConsumerKey, sinaConsumerSecret);
			mWeibo.setRedirectUrl(URL);
			mWeibo.authorize(activity, new LoginSinaListener());

		} else {
			Toast.makeText(context, "新浪微博已登录", Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * 判断新浪微博是否登录
	 */
	@Override
	public void isLogin() {
		// TODO Auto-generated method stub
		NMGPublicData.SINA_TOKEN = (String)NMGConfigHelper
		.getSharePref(context, NMGPublicData.SHARED_PRE_SAVE_FILE_NAME,
				NMGPublicData.SINA_TOKEN_SAVE_KEY,
				NMGConfigHelper.DATA_STRING);
		NMGPublicData.SINA_SECRET = (String)NMGConfigHelper
		.getSharePref(context, NMGPublicData.SHARED_PRE_SAVE_FILE_NAME,
				NMGPublicData.SINA_SECRET_SAVE_KEY,
				NMGConfigHelper.DATA_STRING);
		if(!NMGPublicData.SINA_TOKEN.equals("")&&NMGPublicData.SINA_TOKEN != null){
			accessToken = new AccessToken(NMGPublicData.SINA_TOKEN, NMGPublicData.SINA_SECRET);
			mWeibo.setAccessToken(accessToken);
			isSinalogin = true;
		}
	}

	/**
	 * 登出新浪微博
	 */
	@Override
	public void snsLogout() {
		// TODO Auto-generated method stub
		if (isSinalogin) {
			mWeibo.setAccessToken(null);
			isSinalogin = false;
			clearSinaWeiboSaveConf();
			Toast.makeText(context, "新浪微博退出成功", Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(context, "新浪微博已退出", Toast.LENGTH_SHORT).show();

		}
	}

	@Override
	public void snsShare() {
		// TODO Auto-generated method stub
	}
	
	/**
	 * 分享新浪微博，不带分享对话框
	 * @param content 分享的文字内容
	 * @param picPath 分享的文字内容
	 */
	public void snsShare(String content,String picPath){
		if (isSinalogin) {
			if (content.length() > 140) {
				content = content.substring(0, 140);
			}
			if(getImage(picPath)){
				shareToSinaWithPic( "", "", content, picPath, mWeibo);
			}else{
				shareToSinaNoPic( "", "", content, mWeibo);
			}
		} else {
			Toast.makeText(context, "请登录新浪微博", Toast.LENGTH_SHORT).show();
		}
	}
	
	/**
	 * 分享新浪微博，带分享对话框
	 * @param content 分享的文字内容
	 * @param picPath 分享的文字内容
	 */
	public void snsShareWithDialog(String content,String picPath){
		if (isSinalogin){
			if (content.length() > 140) {
				content = content.substring(0, 140);
			}
			try {
				mWeibo.share2weibo(activity, NMGPublicData.SINA_TOKEN, NMGPublicData.SINA_SECRET, content, picPath);
			} catch (WeiboException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			Toast.makeText(context, "请登录新浪微博", Toast.LENGTH_SHORT).show();
			}
		
	}

	private class LoginSinaListener implements WeiboDialogListener {

		@Override
		public void onComplete(Bundle values) {
			String token = values.getString("access_token");
			String expires_in = values.getString("expires_in");
			String Secret = mWeibo.getAccessToken().getSecret();
			accessToken = new AccessToken(token, Secret);
			accessToken.setExpiresIn(expires_in);
			mWeibo.setAccessToken(accessToken);
			isSinalogin = true;
			NMGConfigHelper.setSharePref(context,NMGPublicData.SHARED_PRE_SAVE_FILE_NAME,NMGPublicData.SINA_TOKEN_SAVE_KEY, token);
			NMGConfigHelper.setSharePref(context,NMGPublicData.SHARED_PRE_SAVE_FILE_NAME,NMGPublicData.SINA_SECRET_SAVE_KEY, Secret);
		}

		@Override
		public void onWeiboException(WeiboException e) {
			
		}

		@Override
		public void onError(com.weibo.net.DialogError e) {
			
		}

		@Override
		public void onCancel() {
			
		}
	}
	
	
	//清除新浪微博信息
	private void clearSinaWeiboSaveConf(){
		NMGConfigHelper.setSharePref(context,NMGPublicData.SHARED_PRE_SAVE_FILE_NAME,NMGPublicData.SINA_TOKEN_SAVE_KEY, "");
		NMGConfigHelper.setSharePref(context,NMGPublicData.SHARED_PRE_SAVE_FILE_NAME,NMGPublicData.SINA_SECRET_SAVE_KEY, "");
		NMGPublicData.SINA_TOKEN = "";
		NMGPublicData.SINA_SECRET = "";
	}
	
	/**
	 * 带图片的新浪微博分享（不显示分享内容对话框）
	 * @param lon
	 * @param lat
	 * @param content
	 * @param picPath
	 * @param weibo
	 */
	public void shareToSinaWithPic(String lon, String lat,String content, String picPath, Weibo weibo) {
		Toast.makeText(activity, "微博分享中...", Toast.LENGTH_SHORT).show();
		Utility.setAuthorization(new Oauth2AccessTokenHeader());
		WeiboParameters bundle = new WeiboParameters();
		bundle.add("source", sinaConsumerKey);
		bundle.add("status", content);
		bundle.add("pic", picPath);
		if (!TextUtils.isEmpty(lon)) {
			bundle.add("lon", lon);
		}
		if (!TextUtils.isEmpty(lat)) {
			bundle.add("lat", lat);
		}
		// String rlt = "";
		String url = Weibo.SERVER + "statuses/upload.json";
		AsyncWeiboRunner weiboRunner = new AsyncWeiboRunner(weibo);
		weiboRunner.request(context, url, bundle, Utility.HTTPMETHOD_POST,this);
	}

	/**
	 * 不带图片的新浪微博分享（不显示分享内容对话框）
	 * @param lon
	 * @param lat
	 * @param content
	 * @param weibo
	 */
	private void shareToSinaNoPic(String lon, String lat,String content, Weibo weibo) {
		Toast.makeText(activity, "微博分享中...", Toast.LENGTH_SHORT).show();
		Utility.setAuthorization(new Oauth2AccessTokenHeader());
		WeiboParameters bundle = new WeiboParameters();
		bundle.add("source", sinaConsumerKey);
		bundle.add("status", content);
		if (!TextUtils.isEmpty(lon)) {
			bundle.add("lon", lon);
		}
		if (!TextUtils.isEmpty(lat)) {
			bundle.add("lat", lat);
		}
		// String rlt = "";
		String url = Weibo.SERVER + "statuses/update.json";
		AsyncWeiboRunner weiboRunner = new AsyncWeiboRunner(weibo);
		weiboRunner.request(context, url, bundle, Utility.HTTPMETHOD_POST, this);
	}

	@Override
	public void onComplete(String response) {
		// TODO Auto-generated method stub
		activity.runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(activity, "分享成功！",Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	public void onIOException(IOException e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onError(WeiboException e) {
		// TODO Auto-generated method stub
		e.printStackTrace();
		activity.runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(activity, "分享失敗！",Toast.LENGTH_SHORT).show();
			}
		});
	}
	
	/**
	 * 判断是否下载完图片
	 * @param path
	 * @return
	 */
	private boolean getImage(String path) {
		Bitmap imageBitmap = BitmapFactory.decodeFile(path);
		if (imageBitmap != null) {
			return true;
		}
		return false;
	}
}
