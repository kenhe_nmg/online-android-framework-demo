package nmg.online.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import android.content.Context;
import android.os.Environment;
import android.util.Log;

public class NMGFileHelper {
	private static final String TAG = "NMGFileHelper";
	
	/**
	 * 例子：  清除缓存里面的东东
     * File  cacheDir = getCacheDir();
	 *deleteFiles(cacheDir.toString(),false);
	 * @param fileAbsPath
	 * @param isDelDir
	 */
	public static void deleteFiles(String fileAbsPath, boolean isDelDir){	  // isDelDir是否删除目录  
		  File path = new File(fileAbsPath);
		  if(!path.exists())   return ;   
		  if(path.isFile()){   
			  path.delete();	   
		  return;   
		  }	   
		  File[] files = path.listFiles();	       
	      for(int i=0;i<files.length;i++)
	      {   
	    	  deleteFiles(files[i].toString(),isDelDir);   //递归
	      }
	      if(isDelDir)path.delete(); 		        
	}
	
	/**
	 * 程序路径 getFilesDir().getAbsolutePath() cache目錄：getCacheDir()
	 * 创建一个SD卡上的文件夹: dir是文件夹的相对路径
	 * @param relativeDir
	 * @return
	 */
	public static String createSDCardDir(String relativeDir){
	   
		if( isSDCard() )
		{
			File sdcardDir =Environment.getExternalStorageDirectory();				
			String fileDirectory = sdcardDir.getAbsolutePath() + java.io.File.separator + relativeDir; 
			Log.i(TAG,"createSDCardDir(String dir)中SDCard的完整路径是："+ fileDirectory);
			return createDir(fileDirectory);
		}else
		{
			return "noSDCard";
		}	   
	   
    }
	
	/**
	 * 返回相对文件的绝对路径： 返回模式：1：sdcard的绝对路径； 2：程序的文件目录的绝对路径；3：程序的缓存目录的绝对路径 ；4： 数据库的目录绝对路径；
	 * @param context
	 * @param relativeFilePath
	 * @param returnModel
	 * @return
	 */
	public static String returnAbsFilePath(Context context, String relativeFilePath , int returnModel)
	{
		switch (returnModel) {
		case 1: // sd卡路径			
			return (Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + relativeFilePath).toString();
		case 2: // 程序的数据路径			
			return context.getFilesDir().getAbsolutePath() + "/" + relativeFilePath;
		case 3: // 程序的缓存路径			
			return context.getCacheDir().getAbsolutePath() + "/" + relativeFilePath ;
		case 4: //  数据库的路径			
			return context.getDatabasePath(relativeFilePath).getAbsolutePath() ;
		
		default:
			return "false";
		}
		
			
			
			
	}
	
	/**
	 * 判断是否存在SDCard
	 * @return
	 */
	public static boolean isSDCard()
	{
	    //判断是否存在sdcard
	    if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
		  return true;
	    }else
	    {
	    	return false;
	    }
	}
	
	/**
	 * 创建目录,返回String型说明："true"：已经存在此目录；"false"：目录创建失败； 完整路径名称：创建成功
	 * @param absFileDir
	 * @return
	 */
	public static String  createDir(String absFileDir ) {
		  File path = new File(absFileDir);
		  try{
			  // 判断目录是否存在：
			  if (!path.exists()) {
				  //若不存在，创建目录，判断是否创建成功
				  if(path.mkdirs())
				  {					
					  return absFileDir;
				  }else
				  {
					  return "false";
				  }			 
				
			  }else
			  {
				  return "true";
			  }
		  }catch(Exception e)
		  {
			  Log.e(TAG,e.toString());
			  return "false";
		  }	   
	}
	
	/**
	 * 判断文件(包括文件夹)是否已经存在: filePath:文件路径  ，existModel：文件判断的模式
	 * （1：在SDCard中是否存在此文件；2：程序的数据目录下是否存在此文件；3：缓存目录下是否存在此文件，4：把filePath当绝对路径判断;）
	 * @param context
	 * @param filePath
	 * @param existModel
	 * @return
	 */
	public static boolean isFileExist(Context context, String filePath,int existModel)
	{
		switch (existModel) {
		case 1: // 在SDCard中是否存在此文件
			String absSDFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + filePath;
			File path1 = new File(absSDFilePath);
			if(path1.exists()) 
			{
				return true;
			}else 
			{
				return false;
			}
		case 2: // 在程序的数据目录下是否存在此文件
			String absSoftFilePath = context.getFilesDir().getAbsolutePath() + "/" + filePath;
			File path2 = new File(absSoftFilePath);
			if(path2.exists()) 
			{
				return true;
			}else 
			{
				return false;
			}
		case 3: // 在程序的缓存目录下是否存在此文件
			String absCacheFilePath = context.getCacheDir().getAbsolutePath() + "/" + filePath;
			File path3 = new File(absCacheFilePath);
			if(path3.exists()) 
			{
				return true;
			}else 
			{
				return false;
			}
		case 4: // 把filePath当绝对路径判断
			File path4 = new File(filePath);
			if(path4.exists()) 
			{
				return true;
			}else 
			{
				return false;
			}
			
		default:
				return false;
		}
	}
	
	/**
	 * 把文件保存到sd卡或磁盘中，如果成功返回保存的路径，如果保存失败返回"false"，
	 * 保存模式的说明： 1：保存到sdcare ，2：保存到程序的数据目录下，3：先保存到sdcare如果没有sdcare，保存程序的数据目录下，4：保存到程序的缓存目录下
	 * @param context
	 * @param is
	 * @param relativeFileDir
	 * @param fileName
	 * @param saveModel
	 * @return
	 */
	public static String createFile(Context context, InputStream is , String relativeFileDir , String fileName,  int saveModel)
	{
		switch (saveModel) {
		case 1: // 保存到sd卡
			String absSDFileDir = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + relativeFileDir ;	
			Log.i(TAG,"absSDFileDir=" + absSDFileDir );
			File path1 = new File(absSDFileDir);
			if( ! path1.exists() ) 
			{
				path1.mkdirs();
			}
			
			Log.i(TAG,"absSDFileDir + / + fileName=" + absSDFileDir + "/" + fileName );
			
			if(saveFile(is, absSDFileDir + "/" + fileName)) // 文件创建成功
			{
				return absSDFileDir + "/" + fileName;
			}else 
			{
				return "false";
			}					
			
		case 2: // 保存到程序目录下
			String absFileDir = context.getFilesDir().getAbsolutePath() + "/" + relativeFileDir ;
			File path2 = new File(absFileDir);
			if( ! path2.exists() ) 
			{
				path2.mkdirs();
			}
			if(saveFile(is, absFileDir + "/" + fileName))
			{
				return absFileDir + "/" + fileName;
			}else
			{
				return "false";
			}
			
		case 3: // 先保存SDCard，如果没有SDCard，保存到程序目录下			
			if ( isSDCard()) 
			{
				String absSDFileDir2 = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + relativeFileDir ;
				File path3 = new File(absSDFileDir2);
				if( ! path3.exists() ) 
				{
					path3.mkdirs();
				}
				if(saveFile(is, absSDFileDir2 + "/" + fileName))
				{
					return absSDFileDir2 + "/" + fileName;
				}else
				{
					return "false";
				}
			}else
			{
				String absFileDir2 = context.getFilesDir().getAbsolutePath() + "/" + relativeFileDir ;
				File path4 = new File(absFileDir2);
				if( ! path4.exists() ) 
				{
					path4.mkdirs();
				}
				if(saveFile(is, absFileDir2 + "/" + fileName))
				{
					return absFileDir2 + "/" + fileName;
				}else
				{
					return "false";
				}
			}
			
		case 4: // 保存到程序的缓存目录下			
			String absCacheFilePath = context.getCacheDir().getAbsolutePath() + "/" + relativeFileDir ;
			File path5 = new File(absCacheFilePath);
			if( ! path5.exists() ) 
			{
				path5.mkdirs();
			}
			if(saveFile(is, absCacheFilePath + "/" + fileName))
			{
				return absCacheFilePath + "/" + fileName;
			}else
			{
				return "false";
			}
			
		default:
			return "false";
		}		
	}
	
	
	/**
	 * 把文件保存到sd卡或磁盘中，如果成功返回保存的路径，如果保存失败返回"false"，
	 * 保存模式的说明： 1：保存到sdcare ，2：保存到程序的数据目录下，3：先保存到sdcare如果没有sdcare，保存程序的数据目录下，4：保存到程序的缓存目录下
	 * @param context
	 * @param bytes
	 * @param relativeFileDir
	 * @param fileName
	 * @param saveModel
	 * @return
	 */
	public static String createFile(Context context, byte[] bytes , String relativeFileDir , String fileName,  int saveModel)
	{
		InputStream is = null;
		try{
			is = bytes2InputStream(bytes);
			return createFile(context, is , relativeFileDir , fileName,  saveModel);
		}catch(Exception e)
		{
			Log.e(TAG,e.toString());
			return "false";
		}finally
		{
			if(is != null)
			{
				try {
					is.close();
				} catch (IOException e) {
					Log.e(TAG,e.toString());
				}
			}
		}
		
		
		
		
		
	}
	
	/**
	 * 把二进制缓存保存成文件：
	 * @param bytes
	 * @param absFilePath
	 * @return
	 */
	public static boolean saveFile(byte[] bytes, String absFilePath)	{ 
	   	    		
			
	   	    OutputStream os;
			try 
			{
				os = new FileOutputStream(absFilePath);	
				os.write(bytes);				
				os.close();
				
				return true;
			} catch (Exception e) {
				Log.e(TAG, e.toString());			
				return false;
			}
		   	  
	   	}
	
	/**
	 * 把输入流保存成文件： absFilename给绝对路径；返回值说明：1：创建成功，-1：创建失败； 2：文件已经存在
	 * @param is
	 * @param absFilePath
	 * @return
	 */
	public static boolean saveFile(InputStream is, String absFilePath)	{  
		
//		返回值说明：1：创建成功，-1：创建失败； 2：文件已经存在
//		 File path = new File(absFilename);
//		  if(path.exists()) // 文件存在
//		  {
//			  return 2;
//		  }
   	    		
		byte[] bs = new byte[1024];
   		int len;
   	    OutputStream os;
		try 
		{
			os = new FileOutputStream(absFilePath);			
			while((len = is.read(bs))!=-1){
			   	   os.write(bs, 0, len);
			   	  }
			   	  os.close();
			   	  is.close();
			   	  return true;
		} catch (Exception e) {
			Log.e(TAG, e.toString());			
			return false;
		}
	   	  
   	}
	
	/**
	 * 文件复制
	 * @param srcFilePath
	 * @param desFilePath
	 * @return
	 */
	public static boolean copyFile(String srcFilePath,String desFilePath){   
		
		File srcFile = new File(srcFilePath);
		File desFile = new File(desFilePath);	
		
		return copyFile(srcFile,desFile); 		    
		
	}  
	
	/**
	 * 文件复制
	 * @param srcFile
	 * @param desFile
	 * @return
	 */
	public static boolean copyFile(File srcFile,File desFile){   
		   
		  int length=2097152;   
		   FileInputStream in;
		   FileOutputStream out;
		try {
			in = new FileInputStream(srcFile);
			out=new FileOutputStream(desFile);
			byte[] buffer=new byte[length];   
			  while(true){   
			   int ins=in.read(buffer);   
			   if(ins==-1){   
			     in.close();   
			     out.flush();   
			     out.close();   
			    return true;   
			    }else  
			     out.write(buffer,0,ins);   
			   }
		} catch (Exception e) {			
			e.printStackTrace();
			return false;
		}   		    
		     
		}  
	
	/**
	 * 压缩成Zip文件
	 * @param srcFilePath
	 * @param desZipFilePath
	 * @return
	 */
	public   static   boolean  zipFile(String srcFilePath,String desZipFilePath)  {
		        try   {
		         // 创建文件输入流对象 
		         FileInputStream in  =   new  FileInputStream(srcFilePath);  // 0
		         // 创建文件输出流对象 
		         FileOutputStream out  =   new  FileOutputStream(desZipFilePath);  // 1
		         // 创建ZIP数据输出流对象 
		         ZipOutputStream zipOut  =   new  ZipOutputStream(out);
		         // 创建指向压缩原始文件的入口 :如果是： new  ZipEntry(srcFilePath)，那么压缩文件里面是整个srcFilePath的路径与文件		         
		         String srcFileName = srcFilePath.substring(srcFilePath.lastIndexOf('/')+1);
		         ZipEntry entry  =   new  ZipEntry(srcFileName);  // 去掉路径的原始文件名 
		         zipOut.putNextEntry(entry);
		         // 向压缩文件中输出数据 
		          int  nNumber;
		         byte [] buffer  =   new   byte [ 512 ];
		         while  ( (nNumber  =  in.read(buffer))  !=   - 1 )
		          zipOut.write(buffer,  0 , nNumber);
		         // 关闭创建的流对象 
		         zipOut.close();
		   
		        out.close();
		        in.close();
		        return true ;
		       } catch  (IOException e)  {
		    	   Log.e(TAG,e.toString());
		    	   return false ;
		       } 
		   
	} 
	
	/**
	 * 解压缩Zip文件
	 * @param srcZipFilePath
	 * @param desUzipFilePath
	 * @return
	 */
	public static boolean uzipFile( String srcZipFilePath , String desUzipFilePath  ){
        
        try{
        	FileInputStream fis = new FileInputStream(srcZipFilePath);
//        	if((new File(srcZipFilePath)).exists()){
//        		Log.i(TAG,"解压缩Zip文件存在");
//        	}else{
//        		Log.i(TAG,"解压缩Zip文件不存在");
//        	}
            ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
            FileOutputStream fos = new FileOutputStream(desUzipFilePath);
            zis.getNextEntry();//读取下一个 ZIP 文件条目并将流定位到该条目数据的开始处。
            byte[] buffer = new byte[1024];
            int count = 0;
            while ((count = zis.read(buffer)) > 0) {
                fos.write(buffer, 0, count);
            }
            Log.i(TAG,"解压缩Zip文件while结束");
    
            fos.flush();
            fos.close();
            zis.close();
            return true ;
        }catch(Exception e){            
            Log.e(TAG,e.toString());
            return false;
        }        
    }
	
	/**
	 * 将二进制字节数组转换成输入流
	 * @param buf
	 * @return
	 */
	public static final InputStream bytes2InputStream(byte[] buf) {
		return new ByteArrayInputStream(buf);
	}
	
	/**
	 * 将输入流转换成二进制字节数组
	 * @param inStream
	 * @return
	 * @throws IOException
	 */
	public static final byte[] inputStream2bytes(InputStream inStream)	throws IOException 
	{
		ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
		byte[] buff = new byte[100];
		int rc = 0;
		while ((rc = inStream.read(buff, 0, 100)) > 0) {
			swapStream.write(buff, 0, rc);
		}
		byte[] in2b = swapStream.toByteArray();
		return in2b;
	}
	
	/**
	 * 判断文件大小
	 * @param path
	 * @return 
	 */
	
	public static long fileLength(String path) {
		File file = new File(path);
		FileInputStream fis;
       try {
    	   fis = new FileInputStream(file);
		   int fileLen = fis.available();
		   return fileLen;
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
		return -1;
	}
	
	/**
	 * 转换文件大小
	 * @param fileS
	 * @return 
	 */
	 public static String FormetFileSize(long fileS) {
	        DecimalFormat df = new DecimalFormat("#.00");
	        String fileSizeString = "";
	        if (fileS < 1024) {
	            fileSizeString = df.format((double) fileS) + "B";
	        } else if (fileS < 1048576) {
	            fileSizeString = df.format((double) fileS / 1024) + "K";
	        } else if (fileS < 1073741824) {
	            fileSizeString = df.format((double) fileS / 1048576) + "M";
	        } else {
	            fileSizeString = df.format((double) fileS / 1073741824) + "G";
	        }
	        return fileSizeString;
	    }
}
