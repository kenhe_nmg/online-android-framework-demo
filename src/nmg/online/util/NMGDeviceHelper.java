package nmg.online.util;



import java.io.File;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Display;

public class NMGDeviceHelper {
	private static final String TAG = "NMGDeviceHelper";
	
	/**
	 * 取得手机屏幕分辨率:返回值PublicData.screenWidth , PublicData.screenHeight
	 * @param activy
	 */
	public static void getResolution(Activity activy)
	{
		Display display = activy.getWindowManager().getDefaultDisplay();
		NMGPublicData.mScreenWidth = display.getWidth();
		NMGPublicData.mScreenHeight = display.getHeight();
	}
	
	/**
	 *  获取软件的信息：设备id号PublicData.deviceUID、当前程序版本号PublicData.appVersion、设备的自定义名称PublicData.bluetoothDeviceName
	 * @param context
	 */
	public static void getAppInfo(Context context)
	{
		try
		{				
			   // 获取设备的id（IMEI）：
		       TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE); 
		       NMGPublicData.mDeviceUID = tm.getDeviceId().trim();
		       //Log.i(TAG,"设备编号：" + PublicData.deviceUID);
		       
		       // 机器型号：
		       NMGPublicData.mModel = android.os.Build.MODEL ;
		       // 编译器版本号：
		       NMGPublicData.mSDKVersion = android.os.Build.VERSION.SDK ;
		       // 操作系统版本号：
		       NMGPublicData.mOSVersion = android.os.Build.VERSION.RELEASE ;
		       		       
		       // 获取当前程序的版本号、版本代码号、完整包名、名称：
		       PackageManager manager = context.getPackageManager();
		       NMGPublicData.mAppVersion = manager.getPackageInfo(context.getPackageName(), 0).versionName; 
		       NMGPublicData.mVersionCode = manager.getPackageInfo(context.getPackageName(), 0).versionCode; 
		       NMGPublicData.mPackageName = manager.getPackageInfo(context.getPackageName(), 0).packageName; 
		       NMGPublicData.mAppName = manager.getPackageInfo(context.getPackageName(), 0).applicationInfo.loadLabel(manager).toString(); 
//		       packageInfo.applicationInfo.loadLabel(getPackageManager()).toString();
		       
		       // 获取mac地址：
		       WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);  
		       WifiInfo info = wifi.getConnectionInfo();
		       NMGPublicData.mMacAddress = info.getMacAddress();
		       
		       // 获取ip地址：
		       NMGPublicData.mIPAddress = getLocalIpAddress();
		       
		       
		    	// 取得SDCard当前的状态  
		    	String sDcString = android.os.Environment.getExternalStorageState();  		    	   
		    	if (sDcString.equals(android.os.Environment.MEDIA_MOUNTED)) { 		    	   
		    	    // 取得sdcard文件路径  
		    	    File pathFile = android.os.Environment  
		    	            .getExternalStorageDirectory();  		    	   
		    	    android.os.StatFs statfs = new android.os.StatFs(pathFile.getPath());  		    	   
		    	    // 获取SDCard上BLOCK总数  
		    	    long nTotalBlocks = statfs.getBlockCount();  		    	   
		    	    // 获取SDCard上每个block的SIZE  
		    	    long nBlocSize = statfs.getBlockSize();  		    	   
		    	    // 获取可供程序使用的Block的数量  
		    	    long nAvailaBlock = statfs.getAvailableBlocks();  		    	   
		    	    // 获取剩下的所有Block的数量(包括预留的一般程序无法使用的块)  
		    	    long nFreeBlock = statfs.getFreeBlocks();  		
		    	    
		    	    // 计算SDCard 总容量大小MB  
		    	    NMGPublicData.mSDTotalSize = nTotalBlocks * nBlocSize / 1024 / 1024;  		    	   
		    	    // 计算 SDCard 剩余大小MB  
		    	    NMGPublicData.mSDFreeSize = nAvailaBlock * nBlocSize / 1024 / 1024;  
		    	    
		    	    Log.i(TAG,"nAvailaBlock= " + nAvailaBlock* nBlocSize /1024/1024 + " nFreeBlock=" + nFreeBlock* nBlocSize /1024/1024);
		    	}// end of if  
		    	
		    	
		    	// 获取磁盘信息：
		    File path2 = android.os.Environment.getDataDirectory();
		    android.os.StatFs statfs2 = new android.os.StatFs(path2.getPath()); 
		    // 获取SDCard上每个block的SIZE  
    	    long nBlocSize2 = statfs2.getBlockSize();
		    Log.i(TAG,"=" +  statfs2.getBlockCount());
		    NMGPublicData.mTotalSize = statfs2.getBlockCount() * nBlocSize2 / 1024 /1024;
		    NMGPublicData.mFreeSize = statfs2.getFreeBlocks() * nBlocSize2 / 1024 /1024;
		    
		    ActivityManager am = (ActivityManager) context.getSystemService( Context.ACTIVITY_SERVICE );  
		    MemoryInfo mi = new MemoryInfo( );  
		    am.getMemoryInfo( mi );  		    
		    Formatter.formatFileSize( context, mi.availMem );
		    NMGPublicData.mMemoFreeSize = mi.availMem / 1024 / 1024;
		    
		    
		    
		    // 获取设备的自定義名称（android在藍牙启动了才有此功能）：
	       BluetoothAdapter mAdapter= BluetoothAdapter.getDefaultAdapter(); 
	       if(mAdapter.isEnabled()){
	    	   NMGPublicData.mBluetoothDeviceName = mAdapter.getName();
	       }
		    	
		       
		} catch (Exception e)
		{
			// 获取android_id:可能在2.2以前的版本会出问题
			NMGPublicData.mDeviceUID = Settings.Secure.getString(context.getContentResolver(),Settings.Secure.ANDROID_ID);
			Log.e(TAG,e.toString());			
		}
	}
	
	/**
	 * 获取手机IP地址
	 * @return
	 */
	public static String getLocalIpAddress() {  
        try {  
            for (Enumeration<NetworkInterface> en = NetworkInterface  
                    .getNetworkInterfaces(); en.hasMoreElements();) {  
                NetworkInterface intf = en.nextElement();  
                for (Enumeration<InetAddress> enumIpAddr = intf  
                        .getInetAddresses(); enumIpAddr.hasMoreElements();) {  
                    InetAddress inetAddress = enumIpAddr.nextElement();  
                    if (!inetAddress.isLoopbackAddress()) {  
                        return inetAddress.getHostAddress().toString();  
                    }  
                }  
            }  
        } catch (SocketException ex) {  
            Log.e("WifiPreference IpAddress", ex.toString());  
        }  
        return null;  
    }
}
