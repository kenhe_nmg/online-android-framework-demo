package nmg.online.util;

import java.util.ArrayList;
import java.util.List;
import nmg.demo.activity.R;
import nmg.demo.activity.ViewActivity.MyPagerAdapter;
import android.app.Activity;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

public class NMGViewPager {

	private int offset = 0;
	private int currIndex = 0;
	private int bmpW;
	private List<View> listViews;
	private Activity activity;
	private ImageView cursor;
	private ViewPager mPager;
	
	/**
	 * 初始化
	 * @param activity
	 * @param cursor
	 * @param mPager
	 */
	public NMGViewPager(Activity activity,ImageView cursor,ViewPager mPager){
		this.activity = activity;
		this.cursor = cursor;
		this.mPager = mPager;
	}
	
	/**
	 * 初始化动画
	 * @param viewcouser 移动标签的图片
	 */
	public void InitImageView(int viewcouser) {
		bmpW = BitmapFactory.decodeResource(activity.getResources(), viewcouser)
				.getWidth();// 获取图片宽度
		DisplayMetrics dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		int screenW = dm.widthPixels;// 获取分辨率宽度
		offset = (screenW / 3 - bmpW) / 2;// 计算偏移量
		Matrix matrix = new Matrix();
		matrix.postTranslate(offset, 0);
		cursor.setImageMatrix(matrix);// 设置动画初始位置
	}
	
	/**
	 * 初始化ViewPager
	 */
	public void InitViewPager() {
		listViews = new ArrayList<View>();
		LayoutInflater mInflater = activity.getLayoutInflater();
		listViews.add(mInflater.inflate(R.layout.viewpager1, null));
		listViews.add(mInflater.inflate(R.layout.viewpager2, null));
		listViews.add(mInflater.inflate(R.layout.viewpager3, null));
		mPager.setAdapter(new MyPagerAdapter(listViews));
		mPager.setCurrentItem(0);
		mPager.setOnPageChangeListener(new MyOnPageChangeListener());
	}
	
	/**
	 * 头标点击监听
	 */
	public class MyOnClickListener implements OnClickListener {
		private int index = 0;

		public MyOnClickListener(int i) {
			index = i;
		}

		@Override
		public void onClick(View v) {
			mPager.setCurrentItem(index);
		}
	};
	
	/**
	 * View切换监听
	 */
	public class MyOnPageChangeListener implements OnPageChangeListener {

		int one = offset * 2 + bmpW;// 页卡1 -> 页卡2 偏移量
		int two = one * 2;// 页卡1 -> 页卡3 偏移量

		@Override
		public void onPageSelected(int arg0) {
			Animation animation = null;
			switch (arg0) {
			case 0:
				if (currIndex == 1) {
					animation = new TranslateAnimation(one, 0, 0, 0);
				} else if (currIndex == 2) {
					animation = new TranslateAnimation(two, 0, 0, 0);
				}
				break;
			case 1:
				if (currIndex == 0) {
					animation = new TranslateAnimation(offset, one, 0, 0);
				} else if (currIndex == 2) {
					animation = new TranslateAnimation(two, one, 0, 0);
				}
				break;
			case 2:
				if (currIndex == 0) {
					animation = new TranslateAnimation(offset, two, 0, 0);
				} else if (currIndex == 1) {
					animation = new TranslateAnimation(one, two, 0, 0);
				}
				break;
			}
			currIndex = arg0;
			animation.setFillAfter(true);// True:图片停在动画结束位置
			animation.setDuration(300);
			cursor.startAnimation(animation);
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		@Override
		public void onPageScrollStateChanged(int arg0) {
		}
	}
}
