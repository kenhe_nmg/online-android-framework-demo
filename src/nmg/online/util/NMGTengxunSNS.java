package nmg.online.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import org.json.JSONObject;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.tencent.weibo.api.TAPI;
import com.tencent.weibo.constants.OAuthConstants;
import com.tencent.weibo.oauthv2.OAuthV2;
import com.tencent.weibo.webview.OAuthV2AuthorizeWebView;

public class NMGTengxunSNS implements NMGSns{

	public OAuthV2 oAuth;
	private String redirectUri = "http://www.tencent.com/zh-cn/index.shtml";
	private String AppKey = "801115505";
	private String AppSecret = "be1dd1410434a9f7d5a2586bab7a6829";
	private Context context;
	private Activity activity;
	private File file;
	private boolean isTengxun = false;
	private int myRrequestCode = 2;
	public static final String FileName="/data/data/hk.facebook.text/oauth_tengxun.data";
	public static final String FileDir="/data/data/hk.facebook.text/";
	
	public NMGTengxunSNS(Context context,Activity activity){
		this.context = context;
		this.activity = activity;
		
		oAuth=new OAuthV2(redirectUri);
		oAuth.setClientId(AppKey);
		oAuth.setClientSecret(AppSecret);
		
		file = new File(FileName);
		if(!file.exists()){
			new File(FileDir).mkdirs();
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * 判断腾讯微博是否登录
	 */
	@Override
	public void isLogin() {
		// TODO Auto-generated method stub
		FileInputStream fis = null;
    	ObjectInputStream ois = null;
    	try {
    		fis = new FileInputStream(file);
    		ois = new ObjectInputStream(fis);//此处抛出EOFException，原因是独到了流的末尾还是返回空，我们这里直接在异常中将标志位记为false即可。
    		oAuth = (OAuthV2) ois.readObject();
    		if(oAuth != null){
    			isTengxun = true;
    		}
		} catch (Exception e) {
			isTengxun = false;
		} finally{
			if(ois != null){
				try {
					ois.close();
					ois = null;
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
			if(fis != null){
				try {
					fis.close();
					fis = null;
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}	
		}
	}

	/**
	 * 登录腾讯微博
	 */
	@Override
	public void snsLogin() {
		// TODO Auto-generated method stub
		if(!isTengxun){
			Intent intent = new Intent(activity,OAuthV2AuthorizeWebView.class);   
			intent.putExtra("oauth", oAuth);  
			activity.startActivityForResult(intent, myRrequestCode);  
		}
		else{
			Toast.makeText(context, "腾讯微博已登录成功", Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * 退出腾讯微博
	 */
	@Override
	public void snsLogout() {
		// TODO Auto-generated method stub
		if(isTengxun){
			boolean b = file.delete();
			if(b){
				isTengxun = false;
				Toast.makeText(context, "腾讯微博退出成功", Toast.LENGTH_SHORT).show();
			}
		}else{
			Toast.makeText(context, "腾讯微博已退出", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void snsShare() {
		// TODO Auto-generated method stub
	}
	
	/**
	 * 分享腾讯微博内容
	 * @param content
	 */
	public void snsShare(String content){
		if(isTengxun){
			String response = null;
			String ipAddress = NMGDeviceHelper.getLocalIpAddress();
			TAPI tAPI = new TAPI(OAuthConstants.OAUTH_VERSION_2_A);
		    System.out.println("snsShare--token=="+oAuth.getAccessToken());
	        try {
	        	response = tAPI.add(oAuth, "json", content, ipAddress);
//	        	System.out.println("snsShare--response==="+response);
	        	String s=new JSONObject(response).getString("msg");
//	        	System.out.println("snsShare--msg==="+s);
	        	if(s.equals("ok")){
	        		Toast.makeText(context, "腾讯微博发表成功", Toast.LENGTH_SHORT).show();
	        	}else{
	        		Toast.makeText(context, "腾讯微博发表失败", Toast.LENGTH_SHORT).show();
	        	}
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        tAPI.shutdownConnection();
		}else{
			Toast.makeText(context, "请先登录腾讯微博", Toast.LENGTH_SHORT).show();
		}
	}
	
	/**
	 * 登录腾讯微博后返回的数据，放在所调用Activity的onActivityResult()中
	 * 在onActivityResult()中还需加入以下代码：
	 * if (requestCode==2) {
            if (resultCode==OAuthV2AuthorizeWebView.RESULT_CODE)    {
                
            	tengxun.oAuth = (OAuthV2) data.getExtras().getSerializable("oauth");
            	tengxun.snsResult();
                if(tengxun.oAuth.getStatus()==0)
                    Toast.makeText(getApplicationContext(), "腾讯微博登陆成功", Toast.LENGTH_SHORT).show();
            }
        }
	 */
	public void snsResult(){
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		try {
			fos = new FileOutputStream(file);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(oAuth);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			if(oos != null){
				try {
					oos.close();
					oos = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			if(fos != null){
				try {
					fos.close();
					fos = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			}
		}
		isTengxun = true;
	}
	
}
