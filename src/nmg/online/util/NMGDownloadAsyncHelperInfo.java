package nmg.online.util;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
/**
 * 图片下载并显示任务 信息
 * 
 * @author 		ken-he
 * @created		2011-7-21
 * @since		2011-7-21
 */
public class NMGDownloadAsyncHelperInfo {
	public Drawable mDrawable;
	public Context mContext;
	public int mViewCode = 0;
	public final String mUrl;
	public final WeakReference<View> mView;
	
	/**
	 * 构造函数
	 * @param imageUrl		图片URL
	 * @param imageView		显示图片的View
	 */
	public NMGDownloadAsyncHelperInfo(String imageUrl, View imageView,Context context){
		this.mUrl = imageUrl;
		this.mView = new WeakReference<View>(imageView);
		this.mDrawable = null;
		this.mContext = context;
	}
}
