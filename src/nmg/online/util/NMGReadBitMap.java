package nmg.online.util;

import java.io.File;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;

public class NMGReadBitMap {

	/**
	 * 以最省内存的方式读取本地资源的图片
	 * 
	 * @param context
	 * @param resId
	 * @return
	 */
	/**
	 * 尽量不要使用setImageBitmap或setImageResource或BitmapFactory.decodeResource来设置一张大图
	 * 因为这些函数在完成decode后，最终都是通过java层的createBitmap来完成的，需要消耗更多内存。
	 * 因此，改用先通过BitmapFactory.decodeStream方法，创建出一个bitmap，再将其设为ImageView的 source，
	 * decodeStream最大的秘密在于其直接调用JNI>>nativeDecodeAsset()来完成decode，
	 * 无需再使用java层的createBitmap，从而节省了java层的空间。
	 * 如果在读取时加上图片的Config参数，可以跟有效减少加载的内存，从而跟有效阻止抛out of Memory异常
	 * 另外，decodeStream直接拿的图片来读取字节码了， 不会根据机器的各种分辨率来自动适应，
	 * 使用了decodeStream之后，需要在hdpi和mdpi，ldpi中配置相应的图片资源，
	 * 否则在不同分辨率机器上都是同样大小（像素点数量），显示出来的大小就不对了。
	 */
	public static  Bitmap readBitMap(Context context, int resId) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		options.inPurgeable = true;
		options.inInputShareable = true;
		// 获取资源图片
		InputStream is = context.getResources().openRawResource(resId);
		Bitmap bmp = BitmapFactory.decodeStream(is, null, options);
		return bmp;
	}
    
    /** 
     * 获取SD卡上的所有图片文件 
     * @return 
     */ 
	public static List<String> getSD(String mpath,HashMap<String, SoftReference<Bitmap>> imageCache) { 
        /* 设定目前所在路径 */ 
        File fileK ; 
        List<String> it = new ArrayList<String>();
        System.out.println("mmm===="+mpath);
        fileK = new File(mpath); 
        File[] files = fileK.listFiles(); 
        if(files != null && files.length>0){ 
            for(File f : files ){ 
                if(getImageFile(f.getName())){ 
                    it.add(f.getPath()); 
                     
                    Options bitmapFactoryOptions = new BitmapFactory.Options(); 
                    
                    //下面这个设置是将图片边界不可调节变为可调节 
                    bitmapFactoryOptions.inJustDecodeBounds = true; 
                    bitmapFactoryOptions.inSampleSize = 5; 
                    float imagew = 150; 
                    float imageh = 150; 
                    int yRatio = (int) Math.ceil(bitmapFactoryOptions.outHeight 
                            / imageh); 
                    int xRatio = (int) Math 
                            .ceil(bitmapFactoryOptions.outWidth / imagew); 
                    if (yRatio > 1 || xRatio > 1) { 
                        if (yRatio > xRatio) { 
                            bitmapFactoryOptions.inSampleSize = yRatio; 
                        } else { 
                            bitmapFactoryOptions.inSampleSize = xRatio; 
                        } 
 
                    }  
                    bitmapFactoryOptions.inJustDecodeBounds = false; 
                     
                    Bitmap bitmap = BitmapFactory.decodeFile(f.getPath(), 
                            bitmapFactoryOptions); 
                     
                    //bitmap = BitmapFactory.decodeFile(f.getPath());  
                    SoftReference<Bitmap> srf = new SoftReference<Bitmap>(bitmap); 
                    imageCache.put(f.getName(), srf); 
                } 
            } 
        } 
        return it; 
    } 
	
	
	/** 
     * 获取图片文件方法的具体实现  
     * @param fName 
     * @return 
     */ 
	public static boolean getImageFile(String fName) { 
        boolean re; 
 
        /* 取得扩展名 */ 
        String end = fName 
                .substring(fName.lastIndexOf(".") + 1, fName.length()) 
                .toLowerCase(); 
 
        /* 按扩展名的类型决定MimeType */ 
        if (end.equals("jpg") || end.equals("gif") || end.equals("png") 
                || end.equals("jpeg") || end.equals("bmp")) { 
            re = true; 
        } else { 
            re = false; 
        } 
        return re; 
    } 
}
