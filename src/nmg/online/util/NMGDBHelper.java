package nmg.online.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * 数据库工具类
 * @author ken-he
 * @created	2011-7-18
 * @since	2010-7-20
 */
public class NMGDBHelper extends SQLiteOpenHelper{

	private static String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS %s (%s);";
	private static final String DATABASE_NAME = "nmg_database.db";
	private static final int DATABASE_VERSION = 3;

	
	private static NMGDBHelper helper;
	
	// 构造函数里面创建数据库
	private NMGDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	/**
	 * 获取实例对象
	 * @param context
	 * @return
	 */
	public static NMGDBHelper getInstance(Context context)
	{
		if(helper == null)
		{
			helper = new NMGDBHelper(context);
		}
		return helper;
	}
	
	/**
	 * 创建数据表
	 * @param db
	 * @param tableName
	 * @param strsql
	 */
	public static void createTable(SQLiteDatabase db , String tableName , String strsql)
	{
		String sql = String.format(CREATE_TABLE, tableName , strsql);
		db.execSQL(sql);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// 获取数据表结构资源
		Map<String , String> tables = NMGPublicData.getTables();
		for(String tableName : tables.keySet())
		{
			// 创建数据表
			createTable(db, tableName, tables.get(tableName));
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		Log.v("ken", "upgrade database file.");
		if (oldVersion < newVersion){
			db.beginTransaction();
			try{
				if (oldVersion != newVersion){
					Log.w("ken", "Destroying all old data.");
					Map<String , String> tables = NMGPublicData.getTables();
					for(String tableName : tables.keySet())
					{
						db.execSQL("DROP TABLE IF EXISTS " + tableName + ";");
					}
				}
				onCreate(db);
				db.setTransactionSuccessful();
			}
			catch (SQLException localSQLException){
				Log.e("ken", "Update DB failed." + localSQLException);
			}
			finally{
				db.endTransaction();
			}
		}
	}
	
	/**
	 * 批量插入数据记录
	 * @param tableName
	 * @param lists
	 */
	public void insert(String tableName , List<?> lists)
	{
		
		SQLiteDatabase db = helper.getWritableDatabase();
		for(Object obj : lists)
		{
			Map<String, String> map = NMGDataParser.getFieldValueMap(obj);
			insert(tableName, db, map);
		}
		db.close();
	}
	
	/**
	 * 插入一条数据记录
	 * @param tableName
	 * @param obj
	 * @param retKey
	 * @return	返回指定字段
	 */
	public String insert(String tableName, Object obj , String retKey)
	{
		SQLiteDatabase db = helper.getWritableDatabase();
		Map<String, String> map = NMGDataParser.getFieldValueMap(obj);
		insert(tableName, db, map);
		db.close();
		return map.get(retKey);
	}

	/**
	 * 插入数据库操作
	 * @param tableName
	 * @param db
	 * @param map
	 */
	private void insert(String tableName, SQLiteDatabase db,
			Map<String, String> map) {
		ContentValues values = new ContentValues();
		for(String key : map.keySet())
		{
			values.put(key, map.get(key));
		}
		try {
			db.insertOrThrow(tableName, null, values);
		} catch (SQLiteConstraintException e) {
			// TODO: handle exception
//			db.update(tableName, values, null, null);
		}
	}
	
	/**
	 * 删除指定条件数据记录
	 * @param tableName
	 * @param whereParams
	 * @param whereVal
	 */
	public void delete(String tableName , String[] whereParams , String[] whereVal)
	{
		SQLiteDatabase db = helper.getWritableDatabase();
		String where = null;
		if(whereParams != null && whereParams.length > 0)
		{
			StringBuilder sqlWhere = new StringBuilder();
			if(whereParams.length > 1)
			{
				for(int i = 0;i < whereParams.length;i++)
				{
					sqlWhere.append(whereParams[i]).append("=").append("?").append(" AND ");
				}
				where = sqlWhere.substring(0 , sqlWhere.lastIndexOf("AND"));
			}else
			{
				where = sqlWhere.append(whereParams[0]).append("=").append("?").toString();
			}
		}
		db.delete(tableName, where, whereVal);
		db.close();
	}
	
	/**
	 * 更新指定条件数据记录
	 * @param tableName
	 * @param params
	 * @param values
	 * @param whereParams
	 * @param whereVal
	 */
	public void update(String tableName , String[] params , String[] values , String[] whereParams , String[] whereVal)
	{
		StringBuilder sqlSet = new StringBuilder();
		StringBuilder sqlWhere = new StringBuilder();
		String set = null;
		String where = null;
		String sql = "UPDATE %s SET %s";
		SQLiteDatabase db = helper.getWritableDatabase();
		if(params.length > 0)
		{
			if(params.length > 1)
			{
				for(int i = 0;i < params.length;i++)
				{
					sqlSet.append(params[i]).append("='").append(values[i]).append("'").append(" , ");
				}
				set = sqlSet.substring(0 , sqlSet.lastIndexOf(","));
			}else
			{
				set = sqlSet.append(params[0]).append("='").append(values[0]).append("'").toString();
			}
		}
		if(whereParams != null && whereParams.length > 0)
		{
			sql += " WHERE %s";
			if(whereParams.length > 1)
			{
				for(int i = 0;i < whereParams.length;i++)
				{
					sqlWhere.append(whereParams[i]).append("='").append(whereVal[i]).append("'").append(" AND ");
				}
				where = sqlWhere.substring(0 , sqlWhere.lastIndexOf("AND"));
			}else
			{
				where = sqlWhere.append(whereParams[0]).append("='").append(whereVal[0]).append("'").toString();
			}
			db.execSQL(String.format(sql, tableName , set , where));
		}else
		{
			db.execSQL(String.format(sql, tableName , set));
		}
		db.close();
	}
	
	
	/**
	 * 查询某表中的所有记录
	 * @param <T>
	 * @param tableName	数据表名
	 * @param bean		获取数据对象类型
	 * @return			返回泛型集合
	 */
	@SuppressWarnings("rawtypes")
	public <T> List<T> query(String tableName , Class bean)
	{
		List<T> lists;
		SQLiteDatabase db = helper.getReadableDatabase();
		Cursor cursor = db.query(tableName, null, null, null, null, null, null);
		T current = null;
		lists = getCursorData(bean , cursor, current);
		db.close();
		return lists;
	}

	/**
	 * 获取所有记录数据
	 * @param <T>
	 * @param cls
	 * @param cursor
	 * @param current
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private <T> List<T> getCursorData(Class cls , Cursor cursor, T current) {
		List<T> lists = new ArrayList<T>();
		if(cursor.getCount() > 0)
		{
			cursor.moveToPosition(-1);
			while (cursor.moveToNext()) {
				current = setValue(cls, current, cursor);
				lists.add(current);
				current = null;
			}
		}
		return lists;
	}
	
	/**
	 * 查询指定条件数据
	 * @param <T>
	 * @param tableName
	 * @param params
	 * @param values
	 * @param cls
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public <T> List<T> query(String tableName , String[] params , String[] values , Class cls)
	{
		List<T> lists;
		SQLiteDatabase db = helper.getReadableDatabase();
		StringBuilder sqlstr = new StringBuilder();
		String selection = null;
		T current = null;
		if(params.length > 1)
		{
			for(int i = 0;i < params.length;i++)
			{
				sqlstr.append(params[i]).append("=").append("?").append(" AND ");
			}
			selection = sqlstr.substring(0, sqlstr.lastIndexOf("AND"));
		}else
		{
			selection = sqlstr.append(params[0]).append("=").append("?").toString();
		}
		Cursor cursor = db.query(tableName, null, selection, values, null, null, null);
		lists = getCursorData(cls , cursor, current);
		db.close();
		return lists;
	}

	/**
	 * 对象赋值
	 * @param <T>
	 * @param cls
	 * @param current
	 * @param cursor
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private <T> T setValue(Class cls, T current, Cursor cursor) {
		try {
			current = (T) cls.newInstance();
			Class clazz = current.getClass();
			Field[] fields = clazz.getDeclaredFields();
			Method[] methods = clazz.getDeclaredMethods();
			for(Field field : fields)
			{
				String fieldSetName = NMGDataParser.parSetName(field.getName());
				Method fieldSetMet = null;
				if (!NMGDataParser.checkSetMet(methods, fieldSetName)) {
					System.out.println("No this method!");
					break;
				}  
				int index = cursor.getColumnIndex(field.getName());
				fieldSetMet = clazz.getMethod(fieldSetName, field.getType());
				String type = field.getType().getSimpleName();
				if ("String".equals(type)) {
					fieldSetMet.invoke(current, cursor.getString(index));
				} else if ("Integer".equals(type)  
						|| "int".equals(type)) {
					fieldSetMet.invoke(current, cursor.getInt(index)); 
				} else if ("Long".equalsIgnoreCase(type)) {  
					fieldSetMet.invoke(current, cursor.getLong(index));
				} else if ("Double".equalsIgnoreCase(type)) {  
					fieldSetMet.invoke(current, cursor.getDouble(index));
				} else if ("Boolean".equalsIgnoreCase(type)) {  
					fieldSetMet.invoke(current, Boolean.parseBoolean(cursor.getString(index)));
				} else {  
					System.out.println("not supper type" + type);  
				}
			}
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return current;
	}
	
	/**
	 * 查询记录数
	 * @param table
	 * @return
	 */
	public int getTableCount(String table){
		SQLiteDatabase db = helper.getReadableDatabase();
		Cursor cursor =db.rawQuery("select count( * )from  "+table, null );
		cursor.moveToFirst();
		int count = cursor.getInt(0);
		cursor.close();
		db.close();
		return count;
	}
	
}
