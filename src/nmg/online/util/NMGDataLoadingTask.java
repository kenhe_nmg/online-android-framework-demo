package nmg.online.util;

import java.io.Serializable;
import nmg.online.ui.IJsonAdapter;
import android.os.AsyncTask;

/**
 * 
 * @author TenZhuang
 *
 */

public class NMGDataLoadingTask extends AsyncTask<String, Integer, Serializable>{

	private	IJsonAdapter mAdapter;
	private Class<? extends Serializable> mClass;
	
	public NMGDataLoadingTask(IJsonAdapter caller, Class<? extends Serializable> mclass) {
		mAdapter = caller;
		mClass = mclass;
	}
	
	@Override
	protected Serializable doInBackground(String... params) {
		// TODO Auto-generated method stub
		String jsonString;
		Serializable data = null;
		try{
			jsonString = NMGHttpHelper.post(params[0], params[1], params[2]);
			if(jsonString == null || NMGHttpHelper.getResponseCode() == -1)
			{
				return null;
			}else{
				data = NMGDataParser.getBody(jsonString, mClass);
			}
			
			if(data == null)
			{
				return null;
			}
			return data;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 取消 获取信息
	 * 执行 cancel回调
	 */
	@Override
	protected void onCancelled() {
		super.onCancelled();
		mAdapter.onCancelled();
	}
	
	/**
	 * 执行前
	 * 执行 Begin 回调
	 */
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		mAdapter.onBegin();
	}

	/**
	 * 执行后, Call 回调 onFinished
	 */
	@Override
	protected void onPostExecute(Serializable result) {
		super.onPostExecute(result);
		mAdapter.onFinished(this, result);
//		SocketUtil.closeSocket();
		try {
			this.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
