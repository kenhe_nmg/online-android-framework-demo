package nmg.online.util;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;



public class NMGPublicData
{
//	private static final String TAG = "PublicData";
	
	public final static String SHARED_PRE_SAVE_FILE_NAME = "setting";
	//Facebook
	public final static String FB_APPID = "";
	public static String FACEBOOK_ACCESS_TOKEN;
	public static Long FACEBOOK_ACCESS_EXPIRES;
	public final static String FB_ACCESS_TOKEN_SAVE_KEY = "fbAccesToken";        // 保存facebook的AccessToken的key 
	public final static String FB_ACCESS_EXPIRES_SAVE_KEY = "fbAccessExpires";   // 保存facebook的AccessExpires的key 
	//新浪微博
	public final static String SINA_CONSUMER_KEY = "";
	public final static String SINA_CONSUMER_SECRET ="";
	public static String SINA_TOKEN;
	public static String SINA_SECRET;
	public final static String SINA_TOKEN_SAVE_KEY = "sinaAccesToken";
	public final static String SINA_SECRET_SAVE_KEY = "sinasecert";
	//twitter
	public static String CONSUMER_KEY = "LGdNG93mjMQKRfs2BoRQ";   // 写你自己在twitter上申请的应用的key：
	public static String CONSUMER_SECRET = "ubh17klWGaepdLgpCu8Ut5cx8voFhvmRvismvlnKWI";   // 写你自己在twitter上申请的应用的secret：
	public static String PREFERENCE_NAME = "twitter_oauth";
	public static final String PREF_KEY_SECRET = "oauth_token_secret";
	public static final String PREF_KEY_TOKEN = "oauth_token";
	public static final String CALLBACK_URL = "myapp://oauth";
	public static final String IEXTRA_AUTH_URL = "auth_url";
	public static final String IEXTRA_OAUTH_VERIFIER = "oauth_verifier";
	public static final String IEXTRA_OAUTH_TOKEN = "oauth_token";
	
	public static String mDeviceUID;  // 設備ID（IMEI）
	public static String mBluetoothDeviceName ; // 機器的設備名字（只能在藍牙裡面設置）由MainTab.java裡面開始時獲取到
	public static String mAppVersion ;   // 软件版本号
	public static int mVersionCode ;   // 软件版本代码号
	public static String mPackageName ;   // 软件完整包名
	public static String mAppName ;      // 软件名称
	public static String mModel ;	    // 手机型号
	public static String mSDKVersion ;   // sdk的开发版本号
	public static String mOSVersion ;    //操作系统版本号
	public static long mSDTotalSize ;    //SD卡总容量，单位Mb
	public static long mSDFreeSize ;    //SD卡剩余容量，单位Mb
	public static long mTotalSize ;      //磁盘总容量，单位Mb
	public static long mFreeSize ;       //磁盘剩余容量，单位Mb
	public static long mMemoFreeSize ;       //可用内存，单位Mb
	public static String mMacAddress ;       //mac地址
	public static String mIPAddress ;       //IP地址
	
    public final static int PTHOTO_SOURCE = 0;    // 原图
    public final static int PTHOTO_FASHION = 1;  // 时尚相册
    public final static int PTHOTO_DANYAN = 2;    // 淡雅
    public final static int PTHOTO_ROMAN = 3;   // 浪漫回忆
    public final static int PTHOTO_LIGHT = 4; // 经典光照
    public final static int PTHOTO_JADE = 5;     // 翡翠
    public final static int PTHOTO_FILM = 6;   // 电影
    public final static int PTHOTO_STAMP = 7;   // 邮票
    public final static int PTHOTO_DREAMTIME = 8;   // 梦幻时光
    public final static int PTHOTO_DARKROOM = 9;    // 暗房
    public final static int PTHOTO_OLDTIME = 10;  // 怀旧
    public final static int PTHOTO_BLACKWHITE = 11;   // 黑白
    public final static int PTHOTO_TILE = 12;  // 瓷砖
    public final static int PTHOTO_RETRO = 13;     // 复古木框
    public final static int PTHOTO_LOMOD = 14;    // Lomo-D
    public final static int PTHOTO_LOMOL = 15;    // Lomo-L
    public final static int PTHOTO_BLANCH = 16;  // 漂白
    public final static int PTHOTO_NOTEPAD = 17; // 记事本
    public final static int PTHOTO_GALANT = 18;    // 华丽相框
    public static final String HTTP_HOST = "www.gytam.newmonday.com.hk";
	
	public static final String IMAGE_DIR = "nmg/icon/";	// 项目图片目录
	
	public static final String APP_INFO_PATH = "http://www.beeweb.hk/mapi/ios/rank/rank.php" ;
//	private static final String GA_Tag_ID = "UA-21081133-1" ;  // 公司的UA-15204427-23；tim9.liu9@gmail.com :  UA-21081133-1
	
	//  獲取全部匯價名稱URL
	public static final String LIST_PRICE__URL = "http://emp888.com/empmp/empmp_all_json.aspx";	
	 //  獲取列表報價URL
//	public static final String LIST_PRICE__URL ="http://192.168.2.4/web/egfs/index.php?c=Index&action=androidGetData";
	
	// 当前标签：5个标签：及时报价、自选报价、公司简介、美国假期、联络我们
	public static int currentTag = 0; // 默认是第一个标签：及时报价
	
	// 繁、简、英	
	public static final String BIG5 = "big5";
	public static final String GB = "gb";
	public static final String ENG = "eng";
	
	// 网络获取数据的刷新时间	
	public static int refreshTimes = 2000;
	
	// 设置里面:设置是否改变通知标识
	public static boolean isSetChangeMainTab = false; // 主页面
	public static boolean isSetChangeDetail = false;  // 详细报价页面	
	public static boolean isSetChangeDisclaimer = false; // 免责声明
	
	
	// 手机的屏幕分辨率的高与宽 	
	public static int mScreenHeight;
	public static int mScreenWidth ;
	public static final int LITTLE_SCREEN_WIDTH = 240;
	public static final int MIDDLE_SCREEN_WIDTH = 320;
	public static final int BIG_SCREEN_WIDTH = 480;
	
	// 当前语言：及对应的链接
	public static String currentLang;
	public static String CURRENT_LANG_ADDR_URL;
	public static String CURRENT_LANG_HOLIDAY_URL;
	public static String CURRENT_LANG_DISCLAIMERR_URL;
	public static String CURRENT_LANG_COMPANY_URL;
	// 字体大小：
	public static final float TEXT_SIZE_MIDDLE = 26.0f; 
	public static final float TEXT_SIZE_BIG = 32.0f; 
	public static float currentTextSize = TEXT_SIZE_MIDDLE; //  当前字体大小

	// xml文档链接：
	// 联系我们的xml
	public static final String ADDR_URL_BIG5 = "http://emp888.com/empmp/address_big5.aspx";
	public static final String ADDR_URL_GB = "http://emp888.com/empmp/address_gb.aspx";
	public static final String ADDR_URL_ENG = "http://emp888.com/empmp/address_eng.aspx";
	// 假期：
	public static final String HOLIDAY_URL_BIG5 = "http://emp888.com/empmp/usholiday_big5.aspx";
	public static final String HOLIDAY_URL_GB = "http://emp888.com/empmp/usholiday_gb.aspx";
	public static final String HOLIDAY_URL_ENG = "http://emp888.com/empmp/usholiday_eng.aspx";
	//免责声明：	Disclaimer
	public static final String DISCLAIMER_URL_BIG5 = "http://emp888.com/empmp/disclaimer_big5.aspx";
	public static final String DISCLAIMER_URL_GB = "http://emp888.com/empmp/disclaimer_gb.aspx";
	public static final String DISCLAIMER_URL_ENG = "http://emp888.com/empmp/disclaimer_eng.aspx";	
	//公司简介：	Company 
	public static final String COMPANY_URL_BIG5 = "http://emp888.com/empmp/company_big5.aspx";
	public static final String COMPANY_URL_GB = "http://emp888.com/empmp/company_gb.aspx";
	public static final String COMPANY_URL_ENG = "http://emp888.com/empmp/company_eng.aspx";
	
	// 国旗图标的部分路径:  完整例子:http://emp888.com/empmp/icon/images/big_iconAUDHKD.png 
	public static final String ICON_COUNTY_URL = "http://emp888.com/empmp/icon/images/big_icon";
	
		
	// Activity:详细报价选择是否启动:
	public static boolean isStartDetailActivity = false;
	
	public static void getCurrentLangURL(String current_lang){
		// 当前语言为简体，则获取简体版的url
		currentLang = current_lang; // 当前设置的语言
		if(GB.equals(current_lang))
		{
			CURRENT_LANG_ADDR_URL = ADDR_URL_GB;
			CURRENT_LANG_HOLIDAY_URL = HOLIDAY_URL_GB;
			CURRENT_LANG_DISCLAIMERR_URL = DISCLAIMER_URL_GB;
			CURRENT_LANG_COMPANY_URL = COMPANY_URL_GB;
		}else if(ENG.equals(current_lang))
		{     // 当前语言为英文，则获取英文版的url
			CURRENT_LANG_ADDR_URL = ADDR_URL_ENG;
			CURRENT_LANG_HOLIDAY_URL = HOLIDAY_URL_ENG;
			CURRENT_LANG_DISCLAIMERR_URL = DISCLAIMER_URL_ENG;
			CURRENT_LANG_COMPANY_URL = COMPANY_URL_ENG;
		} else
		{
			 // 默认为繁体，则获取繁体版的url
			CURRENT_LANG_ADDR_URL = ADDR_URL_BIG5;
			CURRENT_LANG_HOLIDAY_URL = HOLIDAY_URL_BIG5;
			CURRENT_LANG_DISCLAIMERR_URL = DISCLAIMER_URL_BIG5;
			CURRENT_LANG_COMPANY_URL = COMPANY_URL_BIG5;
		}
	}
	
	

	 // 获取客户的语言设置状态,及設置对应的语言和URL
	public static void setCurrentLangAndTextSize(Context context)
	{
		Resources  res;
		Configuration conf;
		DisplayMetrics dm;
	 	SharedPreferences preferences = context.getSharedPreferences("setting", Context.MODE_WORLD_READABLE);
		String lang = preferences.getString("lang", NMGPublicData.BIG5); // 默认是繁体版本			
//		Log.i(TAG,"lang=" + lang);
		res = context.getResources();
		conf = res.getConfiguration();
		dm = res.getDisplayMetrics();
		if(lang.equals(NMGPublicData.GB)) // 简体中文
		{			
			conf.locale = Locale.SIMPLIFIED_CHINESE;
		}else if(lang.equals(NMGPublicData.ENG)) // 英文
		{			
			conf.locale = Locale.ENGLISH;
		}else // 默认繁体中文
		{			
			conf.locale = Locale.TRADITIONAL_CHINESE;
		}
		
		res.updateConfiguration(conf,dm); 
		// 设置链接获取的数据URL：简、繁、英
		NMGPublicData.getCurrentLangURL(lang);	
//			String loc = Locale.getDefault().getLanguage();
//			Log.i(TAG, loc); // zh  en			
//	    Log.i(TAG,"R.string.hello=" + context.getString(R.string.hello));
		
		String textSize = preferences.getString("textsize", "middle"); // 默认是适中字体
		if(textSize.equals("big"))
		{
			NMGPublicData.currentTextSize = NMGPublicData.TEXT_SIZE_BIG;
		}else
		{
			NMGPublicData.currentTextSize = NMGPublicData.TEXT_SIZE_MIDDLE;
		}
				
	}
   
	/**
	 * 创建数据表	
	 * 以Map集合初始化数据表结构,key为表名,value为数据表结构的语句
	 * @return
	 * @author ken-he
	 */
	public static final Map<String , String> getTables()
	{
		String talbe1 = "_id INTEGER PRIMARY KEY AUTOINCREMENT,id INTEGER UNIQUE,name TEXT,age INTEGER,heigh DOUBLE";
		Map<String, String> tableMaps = new HashMap<String, String>();
		tableMaps.put("texttable", talbe1);
		tableMaps.put("texttable1", talbe1);
		tableMaps.put("texttable2", talbe1);
		return tableMaps;
	}
}