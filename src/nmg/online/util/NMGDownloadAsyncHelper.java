package nmg.online.util;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.net.HttpURLConnection;
import java.util.Hashtable;

import nmg.demo.activity.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;




/**
 * 图片异步下载任务类
 * (如该图片已存在即显示图片,否则通过URL下载图片并显示)
 * 
 * @author 		ken-he 	
 * @created		2011-7-21
 * @since		2011-7-21
 */
public class NMGDownloadAsyncHelper extends AsyncTask<NMGDownloadAsyncHelperInfo, Void, NMGDownloadAsyncHelperInfo[]> {
	private static final String LOG_TAG = "ImageLoadingTask"; 
	
	private static Drawable defaultIcon = null;
	private static Hashtable<Integer, Object> mList;
	
	private NMGDownloadAsyncHelperInfo[] mTaskInfo;

	static {
		mList = new Hashtable<Integer, Object>();
	}
	
	
	/**
	 * 获取默认图标
	 * @param context
	 * @return
	 */
	public static Drawable getDefaultIcon(Context context){
		if (defaultIcon == null) {
			defaultIcon = context.getResources().getDrawable(R.drawable.icon);
			Log.v(LOG_TAG, "get dafultIcon: " + defaultIcon);
		} else {
			Bitmap bitmap = ((BitmapDrawable) defaultIcon).getBitmap();
			if (bitmap.isRecycled()){
				defaultIcon = context.getResources().getDrawable(R.drawable.icon);
			}
		}
		return defaultIcon;
	}
	
	@Override
	protected void onPostExecute(NMGDownloadAsyncHelperInfo[] result) {
		super.onPostExecute(result);
		Object obj = null;
		if (result == null){
			return;
		}
		/*把所有下载完成的图片显示到 ImageView或TextView中*/
		for(NMGDownloadAsyncHelperInfo task: result){
			View view = task.mView.get();
			if (view != null){
				synchronized (view) {
					obj = mList.get(task.mViewCode);
					if (obj != null && obj.equals(this)) {
						mList.remove(task.mViewCode);
					}
				}
				if (obj != null && obj.equals(this)) {
					//mList.remove(task.mViewCode);
					recycleBitmapOfThView(view);
					if (task.mDrawable == null){
						task.mDrawable = getDefaultIcon(view.getContext()); 
					}
					if ( view instanceof ImageView){
						((ImageView)view).setImageDrawable(task.mDrawable);
					}else if (view instanceof TextView){
						((TextView)view).setBackgroundDrawable(task.mDrawable);
					}
					view.setVisibility(View.VISIBLE);
				} else {
					//回收图片对象
					recycleImage(task.mDrawable);
				}
			} else {
				//回收图片对象
				recycleImage(task.mDrawable);
			}
		}
	}
	
	@Override
	protected NMGDownloadAsyncHelperInfo[] doInBackground(NMGDownloadAsyncHelperInfo...imageTasks) {
		Log.i(LOG_TAG, "ImageLoadingTask: doInBackground()");
		String imgName = null;
		if (imageTasks.length == 0){
			return null;
		}
		mTaskInfo = imageTasks;
		/* 循环所有图片任务 加载图片到Drawable中*/
		for(NMGDownloadAsyncHelperInfo task: imageTasks){
//			if(!SettingsActivity.getDisplayIcon(task.mContext)){
//				return null;
//			}
			View view = task.mView.get();
			if (view == null) {
				continue;
			}
			task.mViewCode = view.hashCode();
			
			try {
				Log.v(LOG_TAG, "mList Size:" + mList.size());
				int count = 0;
				while (mList.size() > 8){
					Thread.sleep(3000);
					count++;
					if (count > 4) {
						mList.clear();
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			synchronized (view) {
				//mList.remove(task.mViewCode);
				mList.put(task.mViewCode, this);
			}
			if (task.mUrl != null && task.mUrl.length() > 0){
				imgName = task.mUrl.substring(task.mUrl.lastIndexOf('/') + 1, task.mUrl.length());
				if (imgName.lastIndexOf(".") < 0){
					continue;
				}
				imgName = imgName.substring(0, imgName.lastIndexOf(".")) + ".jpg";
				
				//判断缓存图片是否存在
				if (NMGFileHelper.isFileExist(task.mContext, NMGPublicData.IMAGE_DIR + imgName, 1)){
					//已存在即从本地读取图片
					Drawable drawable = null;
				    try{
				    	drawable = Drawable.createFromPath(NMGFileHelper.returnAbsFilePath(task.mContext, NMGPublicData.IMAGE_DIR + imgName, 1));
				    } catch (OutOfMemoryError e){
				    	e.printStackTrace();
				    }
					SoftReference<Drawable> softRef = new SoftReference<Drawable>(drawable);
					task.mDrawable = softRef.get();
					drawable = null;
				} else {
					//不存在即从网络读取图片
					//第一种: 没有SDCard存在, 即直接读取网络流信息 显示
					//第二种: 有SDCard保存到SDCard 以后从SDCard读取
					System.out.println("# 8888888888");
					//判断是否存储到SDcdard
					if (! NMGNetworkHelper.checkNet(task.mContext)) {
						task.mDrawable = null;
						continue;
					}
					if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
						Bitmap bmp = returnBitMap(task.mUrl);
						if(bmp != null){
							task.mDrawable =  new BitmapDrawable(bmp);
						}
					} else {
						HttpURLConnection conn = NMGHttpHelper.getConn(task.mUrl);
						InputStream in = null;
						String b = "";
						try {
							in = conn.getInputStream();
							b = NMGFileHelper.createFile(task.mContext, in , NMGPublicData.IMAGE_DIR, imgName, 1);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} finally
						{
							if(conn != null)
							{
								conn.disconnect();
							}
							if(in != null)
							{
								try {
									in.close();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
						if(!"false".equals(b.trim())){
							 Drawable drawable = null;
							    try{
							    	drawable = Drawable.createFromPath(NMGFileHelper.returnAbsFilePath(task.mContext, NMGPublicData.IMAGE_DIR + "/" + imgName, 1));
							    } catch (OutOfMemoryError e){
							    	e.printStackTrace();
							    }
							SoftReference<Drawable> softRef = new SoftReference<Drawable>(drawable);
							task.mDrawable = softRef.get();
							drawable = null;
						}
						
					}
				}
			}
		}
		return imageTasks;
	}
	
	/**
	 * 根据接口返回bitmap
	 * @param mUrl
	 * @return
	 */
	private Bitmap returnBitMap(String mUrl) {
		// TODO Auto-generated method stub
		HttpURLConnection conn = NMGHttpHelper.getConn(mUrl);
		InputStream in = null;
		Bitmap bitmap = null;
		try {
			in = conn.getInputStream();
			bitmap = BitmapFactory.decodeStream(in);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally
		{
			if(conn != null)
			{
				conn.disconnect();
			}
			if(in != null)
			{
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return bitmap;
	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
		if (mTaskInfo != null) {
			for (NMGDownloadAsyncHelperInfo task : mTaskInfo) {
				recycleImage(task.mDrawable);
				mList.remove(task.mViewCode);
			}
		}
	}
	
	/**
	 * 清除ImageView 与 TextView 设置的Bitmap对象
	 * @param view
	 * @return
	 */
	public static boolean recycleBitmapOfThView(View view){
		Drawable drawable = null;
		//获取View中的Drawable对象
		if (view instanceof ImageView) {
			drawable = ((ImageView)view).getDrawable();
		} else if (view instanceof TextView){
			drawable = ((TextView)view).getBackground();
		}
		//判断是否为默认图标, 是则跳过
		if (drawable != null && !drawable.equals(defaultIcon)) {
			if (drawable instanceof BitmapDrawable) {
				final Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
				//执行清理Bitmap资源
				recycleImage(bitmap);
				if (view instanceof ImageView) {
					((ImageView)view).setImageBitmap(null);
				} else if (view instanceof TextView){
					((TextView)view).setBackgroundDrawable(null);
				}
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 清理Drawable对象
	 * @param image
	 */
	public static void recycleImage(Drawable image){
		if (image != null) {
			Bitmap bitmap = (((BitmapDrawable) image).getBitmap());
			bitmap.recycle();
		}
	}

	/**
	 * 清理Bitmap对象
	 * @param image
	 */
	public static void recycleImage(Bitmap image){
		if (image != null) {
			image.recycle();
		}
	}
	
	/**
	 * 该View从HashMap中去除
	 * @param view
	 */
	public static void clearViewMap(View view){
		mList.remove(view.hashCode());
	}
}