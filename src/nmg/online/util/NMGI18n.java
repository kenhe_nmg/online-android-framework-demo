package nmg.online.util;

import java.util.Locale;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;

public class NMGI18n {
//	private static final String TAG = "UtilI18n";
	
	// 设置语言后保存的文件名
	public static final String LANG_CONF_FILE = "language";
	// 繁、简、英
	public static final String BIG5 = "big5";
	public static final String GB = "gb";
	public static final String ENG = "eng";
	
	
	
	// 设置语言：Language;将设置结果保存到配置文件里面
	public static boolean saveLangConf(Context context, String configFileName, String Language)
	{
		return NMGConfigHelper.setSharePref(context, configFileName, "lang", Language); // 保存的key是："lang"		
	}
	
	 // 获取客户的语言设置状态,及設置对应的语言和URL
	public static void setI18n(Context context, String configFileName)
	{
		Resources  res;
		Configuration conf;
		DisplayMetrics dm;
	 	SharedPreferences preferences = context.getSharedPreferences(configFileName, Context.MODE_WORLD_READABLE);
		String lang = preferences.getString("lang", BIG5); // 默认是繁体版本			
//		Log.i(TAG,"lang=" + lang);
		res = context.getResources();
		conf = res.getConfiguration();
		dm = res.getDisplayMetrics();
		if(lang.equals(GB)) // 简体中文
		{			
			conf.locale = Locale.SIMPLIFIED_CHINESE;
		}else if(lang.equals(ENG)) // 英文
		{			
			conf.locale = Locale.ENGLISH;
		}else if( lang.equals(BIG5) )// 默认繁体中文
		{			
			conf.locale = Locale.TRADITIONAL_CHINESE;
		}
		
		res.updateConfiguration(conf,dm); 
		
		// 设置链接获取的数据URL：简、繁、英
//		PublicData.getCurrentLangURL(lang);	
				
	}

}
