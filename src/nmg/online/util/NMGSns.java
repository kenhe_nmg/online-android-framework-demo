package nmg.online.util;

public interface NMGSns {

	public void isLogin();
	public void snsLogin();
	public void snsLogout();
	public void snsShare();
}
