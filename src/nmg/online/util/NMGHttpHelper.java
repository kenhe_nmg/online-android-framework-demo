package nmg.online.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Map;

import nmg.online.model.FormFile;
import android.util.Log;

public class NMGHttpHelper {
	
	private final static String TAG = "NMGHttpHelper";
	private static final int CONN_TIMEOUT = 5 * 1000;		// 连接超时时间
	private static final int READ_TIMEOUT = 10 * 1000;		// 读取数据超时时间
	private static int responseCode = -1;
	
	/**
	 * 获取网络连接内容
	 * @param path URL地址
	 * @return 返回字符串
	 */
	public static String getRequestContent(String path)
	{
		HttpURLConnection conn = null;
		InputStream in = null;
		String content = null;
		try {
			URL url = new URL(path);
			conn = setConn(url);
			in = conn.getInputStream();
			content = readStream(in);
			responseCode = conn.getResponseCode();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally
		{
			if(conn != null)
			{
				conn.disconnect();
			}
			if(in != null)
			{
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return content;
	}
	
	/**
	 * 获取CONN连接对象
	 * @param path	网络地址
	 * @return		返回HttpURLConnection
	 */
	public static HttpURLConnection getConn(String path)
	{
		HttpURLConnection conn = null;
		URL url;
		try {
			url = new URL(path);
			conn = setConn(url);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return conn;
	}

	/**
	 * 获取Connection实例及设置
	 * @param url
	 * @return
	 * @throws IOException
	 * @throws ProtocolException
	 */
	private static HttpURLConnection setConn(URL url) throws IOException,
			ProtocolException {
		HttpURLConnection conn;
		conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setDoInput(true);
		conn.setAllowUserInteraction(true);
		// 设置连接超时时间为10000ms
		conn.setConnectTimeout(CONN_TIMEOUT);
		// 设置读取数据超时时间为10000ms
		conn.setReadTimeout(READ_TIMEOUT);
		return conn;
	}
	
	
	/**
	 * 发送Post请求
	 * @param path
	 * @return 返回请求状态码
	 */
	public static String post(String path , String param , String value)
	{
		responseCode = -1;
		HttpURLConnection conn = null;
		DataOutputStream outStream = null;
		String content = null;
		String data = "";
//        String end = "\r\n";
//        String twoHyphens = "--";
//        String boundary = "*****";
        String params[] = param.split(",");
        String values[] = value.split(",");
		try {
			if(params != null)
				data = mergeData(params , values);
			Log.i(TAG , "# data = " + data);
			URL url = new URL(path);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			setHeader(conn);
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 设置连接超时时间为10000ms
			conn.setConnectTimeout(CONN_TIMEOUT);
			if(!"".equals(data.trim()))
			{
				outStream = new DataOutputStream(conn.getOutputStream());
				outStream.write(data.getBytes());
			}
			responseCode = conn.getResponseCode();
			InputStream in = conn.getInputStream();
			content = readStream(in);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally
		{
			if(conn != null)
			{
				conn.disconnect();
			}
		}
		
		return content;
	}
	
	/**
	 * 合并POST请求参数
	 * @param params	参数名
	 * @param values	参数值
	 * @return String 	合并值
	 */
	private static String mergeData(String[] params, String[] values) {
		// TODO Auto-generated method stub
		String data = "";
		for(int i = 0; i < params.length;i++)
		{
			try {
				data += URLEncoder.encode(params[i], "UTF-8") + "=" + URLEncoder.encode(values[i] , "UTF-8") + "&";
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "";
			}
		}
		return data.substring(0 , data.lastIndexOf("&"));
	}
	
    /** 
     * 直接通过HTTP协议提交数据到服务器,实现表单提交功能 
     * @param actionUrl 上传路径 
     * @param params 请求参数 key为参数名,value为参数值 
     * @param file 上传文件 
     */  
    public static String post(String actionUrl, Map<String, String> params, FormFile[] files) {  
        try {
            String BOUNDARY = "---------7d4a6d158c9"; //数据分隔线  
            String MULTIPART_FORM_DATA = "multipart/form-data";  
            System.out.println("actionUrl = " + actionUrl);
            URL url = new URL(actionUrl);  
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();  
            conn.setDoInput(true);//允许输入  
            conn.setDoOutput(true);//允许输出  
            conn.setUseCaches(false);//不使用Cache  
            conn.setRequestMethod("POST");            
            conn.setRequestProperty("Connection", "Keep-Alive");  
            conn.setRequestProperty("Charset", "UTF-8");  
            conn.setRequestProperty("Content-Type", MULTIPART_FORM_DATA + "; boundary=" + BOUNDARY);  
      
            StringBuilder sb = new StringBuilder();  
              
            //上传的表单参数部分，格式请参考文章  
            for (Map.Entry<String, String> entry : params.entrySet()) {//构建表单字段内容  
                sb.append("--");  
                sb.append(BOUNDARY);  
                sb.append("\r\n");  
                sb.append("Content-Disposition: form-data; name=\""+ entry.getKey() + "\"\r\n\r\n");
                sb.append(entry.getValue());  
                sb.append("\r\n");  
                Log.i(TAG , entry.getKey() + " = " + sb.toString());
            }  
            DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());  
            outStream.write(sb.toString().getBytes());//发送表单字段数据  
             
            //上传的文件部分，格式请参考文章  
            for(FormFile file : files){  
                StringBuilder split = new StringBuilder();  
                split.append("--");  
                split.append(BOUNDARY);  
                split.append("\r\n");  
                split.append("Content-Disposition: form-data;name=\""+ file.getFormname()+"\";filename=\""+ file.getFilname() + "\"\r\n");  
                split.append("Content-Type: "+ file.getContentType()+"\r\n\r\n");  
//                byte[] buf = file.getData();
//                FileOutputStream fos = new FileOutputStream(new File("sdcard/wins.jpg"));
//                fos.write(buf, 0, buf.length);
                Log.i(TAG , file.getFormname() + " = " + split.toString());
                outStream.write(split.toString().getBytes());  
                outStream.write(file.getData(), 0, file.getData().length);  
                outStream.write("\r\n".getBytes());  
            }  
            byte[] end_data = ("--" + BOUNDARY + "--\r\n").getBytes();//数据结束标志           
            outStream.write(end_data);  
            outStream.flush();  
            int cah = conn.getResponseCode();  
            Log.i(TAG , "getResponseCode = " + cah);
            
            if (cah != 200) throw new RuntimeException("请求url失败");  
            InputStream is = conn.getInputStream();  
            int ch;  
            StringBuilder b = new StringBuilder();  
            while( (ch = is.read()) != -1 ){  
                b.append((char)ch);  
            }  
            outStream.close();  
            conn.disconnect();  
            return b.toString();  
        } catch (Exception e) {  
            throw new RuntimeException(e);  
        }  
    }
	
	
    /** 
     * 直接通过post提交数据到服务器
     * @param actionUrl 请求路径 
     * @param params 请求参数 key为参数名,value为参数值
     * 
     * @return  服务器返回的json数据
     */
	public static String post(String actionUrl, Map<String, String> params ) 
	throws Exception{
		StringBuilder sb = new StringBuilder("");
		if(params != null && !params.isEmpty()){
			for(Map.Entry<String, String> entry : params.entrySet()){
				sb.append(entry.getKey()).append('=')
				.append(URLEncoder.encode(entry.getValue(), "UTF-8")).append('&');
			}
			sb.deleteCharAt(sb.length() - 1);
		}
		byte[] data = sb.toString().getBytes();
		URL url = new URL(actionUrl);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		setHeader(conn);
		conn.setConnectTimeout(CONN_TIMEOUT);
		conn.setDoOutput(true);  // 通过post方式提交数据，必须要设置允许输出
		conn.setDoInput(true);  // 通过post方式提交数据需要获取返回值，必须要设置允许输入
		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		conn.setRequestProperty("Content-Length", String.valueOf(data.length));
		OutputStream outStream = conn.getOutputStream();
		outStream.write(data);
		outStream.flush();
//		String  returnStr = StreamTool.streamToString(conn.getInputStream(), "UTF-8");
		
		BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String line = null;
		StringBuffer content= new StringBuffer();
		while((line = in.readLine()) != null)
		{		
			content.append(line);
		}		
		outStream.close();	
		in.close() ;
		
		
	
		return content.toString(); // .substring(0, content.toString().length() - 1)
//		if(conn.getResponseCode() == 200){
//			return true;
//		}
//		return false;		
	} // sendPostRequest()结束
    
	
	/**
	 * 发送Post请求
	 * @param path
	 * @return 返回请求状态码
	 */
	public static String post(String path , String params[] , String values[])
	{
		responseCode = -1;
		HttpURLConnection conn = null;
		DataOutputStream outStream = null;
		String content = null;
		String data = "";
       
		try {
			if(params != null)
				data = mergeData(params , values);
			System.out.println("# data = " + data);
			URL url = new URL(path);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			setHeader(conn);
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 设置连接超时时间为10000ms
			conn.setConnectTimeout(CONN_TIMEOUT);
			if(!"".equals(data.trim()))
			{
				outStream = new DataOutputStream(conn.getOutputStream());
				outStream.write(data.getBytes());
			}
			responseCode = conn.getResponseCode();
			InputStream in = conn.getInputStream();
			content = readStream(in);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally
		{
			if(conn != null)
			{
				conn.disconnect();
			}
		}
		
		return content;
	}

	/**
	 * 读取网络二进制数据
	 * @param inStream	二进制数据
	 * @return 返回字符串
	 */
	public static String readStream(InputStream inStream) {
		// TODO Auto-generated method stub
		ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		int len = -1;
		try {
			while( (len = inStream.read(buf)) != -1){
				outSteam.write(buf, 0, len);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally
		{
			try {
				if(outSteam != null)
				{
					outSteam.close();
				}
				if(inStream != null)
				{
					inStream.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return outSteam.toString();
	}

	/**
	 * 获取响应码
	 * @return 返回响应码
	 */
	public static int getResponseCode()
	{
		return responseCode;
	}
	
	/**
	 * 下载请求 头信息
	 * @param con
	 * @created 2011-7-5
	 * @since 	2011-7-5
	 * @author  Ken
	 */
	private static void setHeader(URLConnection con) {
		con.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.3) Gecko/2008092510 Ubuntu/8.04 (hardy) Firefox/3.0.3");
		con.setRequestProperty("Accept", "*/*");
		con.setRequestProperty("Accept-Language", "en-us,en;q=0.7,zh-cn;q=0.3");
		con.setRequestProperty("Accept-Encoding", "aa");
		con.setRequestProperty("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
		con.setRequestProperty("Keep-Alive", "300");
		con.setRequestProperty("Connection", "keep-alive");
//		con.setRequestProperty("If-Modified-Since", "Fri, 02 Jan 2009 17:00:05 GMT");
		con.setRequestProperty("If-None-Match", "\"1261d8-4290-df64d224\"");
		con.setRequestProperty("Cache-Control", "max-age=0");
	}
}
