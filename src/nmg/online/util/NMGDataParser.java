package nmg.online.util;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

import com.google.gson.Gson;


/**
 * 数据解析工具
 * @created	2011-7-11
 * @since	2011-7-12
 * @author 	ken-he
 */
public class NMGDataParser {
	
	/**
	 * 获取网络数据集合
	 * @param <T>
	 * @param inStream 网络数据流
	 * @param bean 泛型实例对象
	 * @return 泛型集合
	 */
	public static <T> List<T> getNetworkData(InputStream inStream , T bean , int mode) {
		
		switch (mode) {
		case 0:
			XmlPullParser parser = Xml.newPullParser();
			try {
				return getXMLContent(inStream, bean, parser);
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		default:
			String jsonString = NMGHttpHelper.readStream(inStream);
			if(jsonString != null || !"".equals(jsonString))
			{
				return getJsonContent(bean, jsonString);
			}
			break;
		}
		return null;
	}

	/**
	 * 获取XML数据内容
	 * @param <T>
	 * @param inStream
	 * @param bean
	 * @param parser
	 * @return
	 * @throws XmlPullParserException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private static <T> List<T> getXMLContent(InputStream inStream, T bean,
			XmlPullParser parser) throws XmlPullParserException,
			IllegalAccessException, InstantiationException, IOException {
		Class<?> cls = bean.getClass();
		Field[] fields = cls.getDeclaredFields();
		Map<String , String> values = new HashMap<String , String>();
		String className = cls.getSimpleName();
		parser.setInput(inStream, "UTF-8");
		int eventType = parser.getEventType();
		T currentPerson = null;
		List<T> persons = null;
		
		while (eventType != XmlPullParser.END_DOCUMENT) {
			switch (eventType) {
			case XmlPullParser.START_DOCUMENT://文档开始事件,可以进行数据初始化处理
				persons = new ArrayList<T>();
				break;
			case XmlPullParser.START_TAG://开始元素事件
				String name = parser.getName();
				if (name.equalsIgnoreCase(className)) {
					currentPerson = (T) cls.newInstance();
//					currentPerson.setId(new Integer(parser.getAttributeValue(null, "id")));
				} else if (currentPerson != null) {
					for(int i = 0; i < fields.length;i++)
					{
						if(name.equalsIgnoreCase(fields[i].getName()))
						{
							values.put(name, parser.nextText());
							break;
						}
					}
				}
				break;
			case XmlPullParser.END_TAG://结束元素事件
				if (parser.getName().equalsIgnoreCase(className) && currentPerson != null) {
					setFieldValue(currentPerson, values);
					persons.add(currentPerson);
					currentPerson = null;
				}
				break;
			}
			eventType = parser.next();
		}
		inStream.close();
		return persons;
	}
	
	/**
	 * 解析Json字符串
	 * @param <T>
	 * @param bean 泛型对象
	 * @param jsonString JSON字符串
	 * @return 返回泛型對象集合
	 */
	@SuppressWarnings("unchecked")
	private static <T> List<T> getJsonContent(T bean, String jsonString) {
		Gson gson = new Gson();
		List<T> list = new ArrayList<T>();
		list.add((T)gson.fromJson(jsonString, bean.getClass()));
		return list;
	}
	
	
	/**
	 * 解析Json字符串
	 * @param <T>
	 * @param jsonString
	 * @param clazz
	 * @return 返回泛型對象
	 */
	public static <T> T getJsonContent(String jsonString , Class<T> clazz)
	{
		Gson gson = new Gson();
		return gson.fromJson(jsonString, clazz);
	}
	
	
    /** 
     * set属性的值到Bean 
     * @param bean 
     * @param valMap 
     */  
    private static void setFieldValue(Object bean, Map<String, String> valMap) {  
        Class<?> cls = bean.getClass();  
        // 取出bean里的所有方法  
        Method[] methods = cls.getDeclaredMethods();  
        Field[] fields = cls.getDeclaredFields();  
  
        for (Field field : fields) {  
            try {  
  
                String fieldSetName = parSetName(field.getName());  
                if (!checkSetMet(methods, fieldSetName)) {  
                    continue;  
                }  
                Method fieldSetMet = cls.getMethod(fieldSetName, field  
                        .getType());  
                String value = valMap.get(field.getName());  
                if (null != value && !"".equals(value)) {  
                    String fieldType = field.getType().getSimpleName();  
                    if ("String".equals(fieldType)) {  
                        fieldSetMet.invoke(bean, value);  
                    } else if ("Date".equals(fieldType)
                    		|| "date".equals(fieldType)) {  
                        Date temp = parseDate(value);  
                        fieldSetMet.invoke(bean, temp);  
                    } else if ("Integer".equals(fieldType)  
                            || "int".equals(fieldType)) {
                        Integer intval = Integer.parseInt(value);  
                        fieldSetMet.invoke(bean, intval);  
                    } else if ("Long".equalsIgnoreCase(fieldType)
                    		|| "long".equals(fieldType)) {  
                        Long temp = Long.parseLong(value);  
                        fieldSetMet.invoke(bean, temp);  
                    } else if ("Double".equalsIgnoreCase(fieldType)
                    		|| "double".equals(fieldType)) {  
                        Double temp = Double.parseDouble(value);  
                        fieldSetMet.invoke(bean, temp);  
                    } else if ("Boolean".equalsIgnoreCase(fieldType)
                    		|| "boolean".equals(fieldType)) {  
                        Boolean temp = Boolean.parseBoolean(value);  
                        fieldSetMet.invoke(bean, temp);  
                    } else {  
                        System.out.println("not supper type" + fieldType);  
                    }  
                }  
            } catch (Exception e) {  
                continue;  
            }  
        }  
  
    } 
    
    /** 
     * 取Bean的属性和值对应关系的MAP 
     * @param bean 
     * @return Map 
     */  
    public static Map<String, String> getFieldValueMap(Object bean) {  
        Class<?> cls = bean.getClass();  
        Map<String, String> valueMap = new HashMap<String, String>();  
        // 取出bean里的所有方法  
        Method[] methods = cls.getDeclaredMethods();  
        Field[] fields = cls.getDeclaredFields();  
  
        for (Field field : fields) {  
            try {  
                String fieldType = field.getType().getSimpleName();  
                String fieldGetName = parGetName(field.getName());  
                if (!checkGetMet(methods, fieldGetName)) {  
                    continue;  
                }  
                Method fieldGetMet = cls  
                        .getMethod(fieldGetName, new Class[] {});  
                Object fieldVal = fieldGetMet.invoke(bean, new Object[] {});  
                String result = null;  
                if ("Date".equals(fieldType)) {  
                    result = fmtDate((Date) fieldVal);  
                } else {  
                    if (null != fieldVal) {  
                        result = String.valueOf(fieldVal);  
                    }  
                }  
                valueMap.put(field.getName(), result);  
            } catch (Exception e) {  
                continue;  
            }  
        }  
        return valueMap;  
  
    }
  
    /** 
     * 格式化string为Date 
     * @param datestr 
     * @return date 
     */  
    private static Date parseDate(String datestr) {  
        if (null == datestr || "".equals(datestr)) {  
            return null;  
        }  
        try {  
            String fmtstr = null;  
            if (datestr.indexOf(':') > 0) {  
                fmtstr = "yyyy-MM-dd HH:mm:ss";  
            } else {  
                fmtstr = "yyyy-MM-dd";  
            }  
            SimpleDateFormat sdf = new SimpleDateFormat(fmtstr, Locale.UK);  
            return sdf.parse(datestr);  
        } catch (Exception e) {  
            return null;  
        }  
    }  
  
    /** 
     * 日期转化为String 
     * @param date 
     * @return date string 
     */  
    public static String fmtDate(Date date) {  
        if (null == date) {  
            return null;  
        }  
        try {  
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",  
                    Locale.US);  
            return sdf.format(date);  
        } catch (Exception e) {  
            return null;  
        }  
    }  
  
    /** 
     * 判断是否存在某属性的 set方法 
     * @param methods 
     * @param fieldSetMet 
     * @return boolean 
     */  
    public static boolean checkSetMet(Method[] methods, String fieldSetMet) {  
        for (Method met : methods) {  
            if (fieldSetMet.equals(met.getName())) {  
                return true;  
            }  
        }  
        return false;  
    }  
  
    /** 
     * 判断是否存在某属性的 get方法 
     * @param methods 
     * @param fieldGetMet 
     * @return boolean 
     */  
    public static boolean checkGetMet(Method[] methods, String fieldGetMet) {  
        for (Method met : methods) {  
            if (fieldGetMet.equals(met.getName())) {  
                return true;  
            }  
        }  
        return false;  
    }  
  
    /** 
     * 拼接在某属性的 set方法 
     * @param fieldName 
     * @return String 
     */  
    public static String parSetName(String fieldName) {  
        if (null == fieldName || "".equals(fieldName)) {  
            return null;  
        }  
        return "set" + fieldName.substring(0, 1).toUpperCase()  
                + fieldName.substring(1);  
    }
    
    /** 
     * 拼接某属性的 get方法 
     * @param fieldName 
     * @return String 
     */  
    public static String parGetName(String fieldName) {  
        if (null == fieldName || "".equals(fieldName)) {  
            return null;  
        }  
        return "get" + fieldName.substring(0, 1).toUpperCase()  
                + fieldName.substring(1);  
    }  
    
    /**
	 * 解析Json字符串
	 * @param <T>
	 * @param jsonString
	 * @param clazz
	 * @return 返回泛型對象
	 */
	public static <T> T getBody(String jsonString , Class<T> clazz)
	{
		Gson gson = new Gson();
		return gson.fromJson(jsonString, clazz);
	}
    
}
