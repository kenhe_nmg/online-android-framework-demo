package nmg.online.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;

/**
 * 
 * @author TenZhuang
 *
 */
public class NMGFacebookSNS implements NMGSns {

	private Context context;
	private Activity activity;
	private static String facebookID = "331133686981342";
	public static Facebook myFacebook = new Facebook(facebookID);
	private boolean isFBlogin = false;
	private LoginListener login = new LoginListener();
	private static final String[] PERMISSIONS = new String[] {
			"publish_stream", "read_stream", "offline_access" };

	public NMGFacebookSNS(Context mcontext, Activity activity) {
		this.context = mcontext;
		this.activity = activity;
	}

	/**
	 * 登录Facebook
	 * 在所调用Activity的onActivityResult()中需加入以下代码：
	 * NMGFacebookSNS.myFacebook.authorizeCallback(requestCode, resultCode, data);
	 */
	@Override
	public void snsLogin() {
		// TODO Auto-generated method stub
		if (isFBlogin) {
			Toast.makeText(context, "已登录Facebook", Toast.LENGTH_SHORT).show();
		} else {
			myFacebook.authorize(activity, PERMISSIONS, login);

		}
	}

	/**
	 * 判断Facebook是否登录
	 */
	@Override
	public void isLogin() {
		// TODO Auto-generated method stub
		NMGPublicData.FACEBOOK_ACCESS_TOKEN = (String) NMGConfigHelper
				.getSharePref(context, NMGPublicData.SHARED_PRE_SAVE_FILE_NAME,
						NMGPublicData.FB_ACCESS_TOKEN_SAVE_KEY,
						NMGConfigHelper.DATA_STRING);
		NMGPublicData.FACEBOOK_ACCESS_EXPIRES = (Long) NMGConfigHelper
				.getSharePref(context, NMGPublicData.SHARED_PRE_SAVE_FILE_NAME,
						NMGPublicData.FB_ACCESS_EXPIRES_SAVE_KEY,
						NMGConfigHelper.DATA_LONG);

		System.out.println("PublicData.FACEBOOK_ACCESS_TOKEN---"
				+ NMGPublicData.FACEBOOK_ACCESS_TOKEN);
		System.out.println("PublicData.FACEBOOK_ACCESS_EXPIRES---"
				+ NMGPublicData.FACEBOOK_ACCESS_EXPIRES);

		if (NMGPublicData.FACEBOOK_ACCESS_TOKEN != null
				&& NMGPublicData.FACEBOOK_ACCESS_EXPIRES != 0) {
			myFacebook.setAccessToken(NMGPublicData.FACEBOOK_ACCESS_TOKEN);
			myFacebook.setAccessExpires(NMGPublicData.FACEBOOK_ACCESS_EXPIRES);
			isFBlogin = true;

		} else {
//			System.out.println("isFBlogin----" + isFBlogin);
		}
	}

	/**
	 * 退出Facebook
	 */
	@Override
	public void snsLogout() {
		// TODO Auto-generated method stub
		if(isFBlogin){
			new AsyncFacebookRunner(myFacebook).logout(context,
					new RequestListener() {
						@Override
						public void onComplete(String response, Object state) {
//							System.out.println("success logout");
							activity.runOnUiThread(new Runnable() {
								public void run() {
									Toast.makeText(context, "Facebook退出成功！",Toast.LENGTH_SHORT).show();
									isFBlogin = false;
									clearFBSaveConf();
								}
							});
						}

						@Override
						public void onIOException(IOException e, Object state) {
//							System.out.println("onIOException");
						}

						@Override
						public void onFileNotFoundException(
								FileNotFoundException e, Object state) {
//							System.out.println("onFileNotFoundException");
						}

						@Override
						public void onMalformedURLException(
								MalformedURLException e, Object state) {
//							System.out.println("onMalformedURLException");
						}

						@Override
						public void onFacebookError(FacebookError e, Object state) {
//							System.out.println("onFacebookError");
						}
					});
		}else{
			Toast.makeText(context, "Facebook并未登录！",Toast.LENGTH_SHORT).show();
		}
		
	}

	@Override
	public void snsShare() {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 分享Facebook内容
	 * @param params
	 */
	public void snsShare(Bundle params){
		if (isFBlogin) {
			myFacebook.dialog(context, "feed", params,new ShareListener());
		} else {
			Toast.makeText(context, "请先登录Facebook",Toast.LENGTH_SHORT).show();
		}
		System.out.println("isFBlogin----"+isFBlogin);
	}

	private class LoginListener implements DialogListener {

		@Override
		public void onComplete(Bundle values) {

//			System.out.println("success login");
			String fbAccessToken = myFacebook.getAccessToken();
			Long fbAccessExpires = myFacebook.getAccessExpires();
//			System.out.println(myFacebook.getAccessToken());
//			System.out.println(myFacebook.getAccessExpires());
			isFBlogin = true;
			NMGConfigHelper.setSharePref(context,NMGPublicData.SHARED_PRE_SAVE_FILE_NAME,
					NMGPublicData.FB_ACCESS_TOKEN_SAVE_KEY, fbAccessToken); // ：AccessToken
			NMGConfigHelper.setSharePref(context,NMGPublicData.SHARED_PRE_SAVE_FILE_NAME,
					NMGPublicData.FB_ACCESS_EXPIRES_SAVE_KEY, fbAccessExpires); // AccessExpires
			Toast.makeText(context, "Facebook登录成功", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onFacebookError(FacebookError e) {
//			System.out.println("onFacebookError");
		}

		@Override
		public void onError(DialogError e) {
//			System.out.println("onError");
		}

		@Override
		public void onCancel() {
//			System.out.println("onCancel");
		}
	}
	
	private class ShareListener implements DialogListener {

		@Override
		public void onComplete(Bundle values) {
			
//			System.out.println("Share_onComplete");
			String va = values.getString("post_id");
//			System.out.println("va==" + va);
			if (va != null) {
				Toast.makeText(context, "Facebook分享成功", Toast.LENGTH_SHORT).show();
			} else {

			}
		}

		@Override
		public void onFacebookError(FacebookError e) {
			
//			System.out.println("Share_onFacebookError");
		}

		@Override
		public void onError(DialogError e) {
			
//			System.out.println("Share_onError");
		}

		@Override
		public void onCancel() {
			
//			System.out.println("Share_onCancel");
		}

	}

	// 清除Facebook信息
	private void clearFBSaveConf() {
		NMGConfigHelper.setSharePref(context,NMGPublicData.SHARED_PRE_SAVE_FILE_NAME,
				NMGPublicData.FB_ACCESS_TOKEN_SAVE_KEY, "");
		NMGConfigHelper.setSharePref(context,NMGPublicData.SHARED_PRE_SAVE_FILE_NAME,
				NMGPublicData.FB_ACCESS_EXPIRES_SAVE_KEY, 0L);
		NMGPublicData.FACEBOOK_ACCESS_TOKEN = "";
		NMGPublicData.FACEBOOK_ACCESS_EXPIRES = 0L;
	}

	
}
