package nmg.online.util;


import twitter4j.TwitterException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

public class TwitterLoginBrowser extends Activity {

	private String oauthToken;
	private String oauthVerifier;
	private String promptStr = "";

	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		LinearLayout linearLayout = new LinearLayout(this);
		WebView webView = new WebView(this);
		linearLayout.addView(webView);
		setContentView(linearLayout);
		WebSettings webSettings = webView.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webView.loadUrl(this.getIntent().getExtras().getString("auth_url"));

		webView.setWebViewClient(new WebViewClient() {

			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);

				if (url != null && url.startsWith(NMGPublicData.CALLBACK_URL)) {
					String[] param = url.split("\\?")[1].split("&");
					if (param[0].startsWith("oauth_token")) {
						oauthToken = param[0].split("=")[1];
					} else if (param[1].startsWith("oauth_token")) {
						oauthToken = param[1].split("=")[1];
					}
					if (param[0].startsWith("oauth_verifier")) {
						oauthVerifier = param[0].split("=")[1];
					} else if (param[1].startsWith("oauth_verifier")) {
						oauthVerifier = param[1].split("=")[1];
					}
					try {
						NMGTwitterSNS.accessToken = NMGTwitterSNS.twitter.getOAuthAccessToken(NMGTwitterSNS.requestToken,oauthVerifier);
						new WebTask().execute();
					} catch (TwitterException e) {
						e.printStackTrace();
//						System.out.println("发生错误了");
					}
				}

			}
		});
	}

	class WebTask extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			String result = "";
			if (oauthToken != null && oauthVerifier != null) {
				// 保存登陆状态
				NMGConfigHelper.setSharePref(TwitterLoginBrowser.this,
						NMGPublicData.SHARED_PRE_SAVE_FILE_NAME,
						NMGPublicData.PREF_KEY_TOKEN,
						NMGTwitterSNS.accessToken.getToken());
				NMGConfigHelper.setSharePref(TwitterLoginBrowser.this,
						NMGPublicData.SHARED_PRE_SAVE_FILE_NAME,
						NMGPublicData.PREF_KEY_SECRET,
						NMGTwitterSNS.accessToken.getTokenSecret());
				
				NMGTwitterSNS.twitter.setOAuthAccessToken(NMGTwitterSNS.accessToken);
				result = "ok";
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (result.equals("ok")) {
				promptStr = "twitter帳號綁定成功！";
				NMGTwitterSNS.isTwitterLogin = true;
			} else {
				promptStr = "twitter帳號綁定失敗！";
			}
			dialog();
		}

	}

	private void dialog() {
		AlertDialog.Builder builder = new Builder(TwitterLoginBrowser.this);
		builder.setTitle("提示：").setMessage(promptStr)
				.setPositiveButton("確認", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						finish();
					}
				});
		builder.create().show();
	};
}
