package nmg.online.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NMGNetworkHelper {
//	private static final String TAG = "UtilDevice";
	
	
	/*
	 *  例： 如果网络设置有问题，打开系统的网络设置页面
	 * if(checkNet(this))
	   { //如果网络检查成功
			Intent it=new Intent("com.nmg.logic.MainService");
			this.startService(it);
	   }
	 * 
	 */
	// 检查网络连接成功
	public static boolean checkNet(Context context)
	{// 获取手机所有连接管理对象（包括对wi-fi,net等连接的管理） 
	    try { 
	        ConnectivityManager connectivity = (ConnectivityManager) context 
	                .getSystemService(Context.CONNECTIVITY_SERVICE); 
	        if (connectivity != null) { 
	            // 获取网络连接管理的对象 
	            NetworkInfo info = connectivity.getActiveNetworkInfo(); 
	            if (info != null&& info.isConnected()) { 
	                // 判断当前网络是否已经连接 
	                if (info.getState() == NetworkInfo.State.CONNECTED) { 
	                    return true; 
	                }        }        } 
	    } catch (Exception e) { 
	} 
	      return false; 
	}
	
	
	/**
	 * 检查网络的连接方式：WIFI 还是MOBILE: gprs (或者3G)、WIMAX 失败：是"false"
	 * @param context
	 * @return
	 */
	public static String connectModel(Context context)
	{
		ConnectivityManager connec =  (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE); 
		
//		NetworkInfo[] mNetworkInfos = connec.getAllNetworkInfo();
//		for(int i=0; i<mNetworkInfos.length; i++){
//        	Log.i(TAG,"mNetworkInfos[i]网络状态：" + mNetworkInfos[i].toString());
//        }
//        Log.i(TAG,"connec.getAllNetworkInfo网络状态：" + connec.getAllNetworkInfo());
//        Log.i(TAG,"connec.getActiveNetworkInfo网络状态：" + connec.getActiveNetworkInfo());
        
        if (connec.getNetworkInfo(ConnectivityManager.TYPE_WIFI ).getState() == NetworkInfo.State.CONNECTED)  
           return "WIFI";  
        if (connec.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED)  
        	 return "MOBILE"; 
        if (connec.getNetworkInfo(ConnectivityManager.TYPE_WIMAX).getState() == NetworkInfo.State.CONNECTED)  
        	return "WIMAX";        
        
        return "false" ;
	}
	
}
