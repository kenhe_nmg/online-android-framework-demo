package nmg.demo.activity;

import java.io.File;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.Toast;

public class PhotoScanActivity extends Activity{
	private Gallery gallery ; 
    private List<String> ImageList; 
    private List<String> it ; 
    private ImageAdapter adapter ;  
    private HashMap<String, SoftReference<Bitmap>> imageCache = null;
    private Bitmap bitmap = null; 
    private SoftReference<Bitmap> srf = null; 
    private String mpath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/DCIM/Camera/";
    
    @Override 
    public void onCreate(Bundle savedInstanceState) { 
        super.onCreate(savedInstanceState); 
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,  
        WindowManager.LayoutParams.FLAG_FULLSCREEN);  
        setContentView(R.layout.photoscan); 
        init(); 
    } 
     
    private void init(){ 
        imageCache = new HashMap<String, SoftReference<Bitmap>>(); 
         gallery = (Gallery)findViewById(R.id.gallery); 
         ImageList = getSD(); 
         if(ImageList.size() == 0){ 
            Toast.makeText(getApplicationContext(), "无照片，请返回拍照后再使用预览", Toast.LENGTH_SHORT).show(); 
            return ; 
         } 
         adapter = new ImageAdapter(this, ImageList); 
         gallery.setAdapter(adapter); 
         gallery.setOnItemLongClickListener(longlistener); 
    } 
 
     
    /** 
     * Gallery长按事件操作实现 
     */ 
    private OnItemLongClickListener longlistener = new OnItemLongClickListener() { 
 
        @Override 
        public boolean onItemLongClick(AdapterView<?> parent, View view, 
                final int position, long id) { 
            //此处添加长按事件删除照片实现 
            AlertDialog.Builder dialog = new AlertDialog.Builder(PhotoScanActivity.this); 
            dialog.setIcon(R.drawable.icon); 
            dialog.setTitle("删除提示"); 
            dialog.setMessage("你确定要删除这张照片吗？"); 
            dialog.setPositiveButton("确定", new DialogInterface.OnClickListener() { 
                @Override 
                public void onClick(DialogInterface dialog, int which) { 
                    File file = new File(it.get(position)); 
                    boolean isSuccess; 
                    if(file.exists()){ 
                        isSuccess = file.delete(); 
                        if(isSuccess){ 
                            ImageList.remove(position); 
                            adapter.notifyDataSetChanged(); 
                            //gallery.setAdapter(adapter); 
                            if(ImageList.size() == 0){ 
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.phoSizeZero), Toast.LENGTH_SHORT).show(); 
                            } 
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.phoDelSuccess), Toast.LENGTH_SHORT).show(); 
                        } 
                    } 
                } 
            }); 
            dialog.setNegativeButton("取消",new DialogInterface.OnClickListener() { 
                @Override 
                public void onClick(DialogInterface dialog, int which) { 
                    dialog.dismiss(); 
                } 
            }); 
            dialog.create().show(); 
            return false; 
        } 
    }; 
     
    /** 
     * 获取SD卡上的所有图片文件 
     * @return 
     */ 
    private List<String> getSD() { 
        /* 设定目前所在路径 */ 
        File fileK ; 
        it = new ArrayList<String>(); 
        fileK = new File(mpath); 
        File[] files = fileK.listFiles(); 
        if(files != null && files.length>0){ 
            for(File f : files ){ 
                if(getImageFile(f.getName())){ 
                    it.add(f.getPath()); 
                     
                    Options bitmapFactoryOptions = new BitmapFactory.Options(); 
                    
                    //下面这个设置是将图片边界不可调节变为可调节 
                    bitmapFactoryOptions.inJustDecodeBounds = true; 
                    bitmapFactoryOptions.inSampleSize = 5; 
//                    int outWidth  = bitmapFactoryOptions.outWidth; 
//                    int outHeight = bitmapFactoryOptions.outHeight; 
                    float imagew = 150; 
                    float imageh = 150; 
                    int yRatio = (int) Math.ceil(bitmapFactoryOptions.outHeight 
                            / imageh); 
                    int xRatio = (int) Math 
                            .ceil(bitmapFactoryOptions.outWidth / imagew); 
                    if (yRatio > 1 || xRatio > 1) { 
                        if (yRatio > xRatio) { 
                            bitmapFactoryOptions.inSampleSize = yRatio; 
                        } else { 
                            bitmapFactoryOptions.inSampleSize = xRatio; 
                        } 
 
                    }  
                    bitmapFactoryOptions.inJustDecodeBounds = false; 
                     
                    bitmap = BitmapFactory.decodeFile(f.getPath(), 
                            bitmapFactoryOptions); 
                     
                    //bitmap = BitmapFactory.decodeFile(f.getPath());  
                    srf = new SoftReference<Bitmap>(bitmap); 
                    imageCache.put(f.getName(), srf); 
                } 
            } 
        } 
        return it; 
    } 
     
    /** 
     * 获取图片文件方法的具体实现  
     * @param fName 
     * @return 
     */ 
    private boolean getImageFile(String fName) { 
        boolean re; 
 
        /* 取得扩展名 */ 
        String end = fName 
                .substring(fName.lastIndexOf(".") + 1, fName.length()) 
                .toLowerCase(); 
 
        /* 按扩展名的类型决定MimeType */ 
        if (end.equals("jpg") || end.equals("gif") || end.equals("png") 
                || end.equals("jpeg") || end.equals("bmp")) { 
            re = true; 
        } else { 
            re = false; 
        } 
        return re; 
    } 
     
    public class ImageAdapter extends BaseAdapter{ 
        /* 声明变量 */ 
        int mGalleryItemBackground; 
        private Context mContext; 
        private List<String> lis; 
         
        /* ImageAdapter的构造符 */ 
        public ImageAdapter(Context c, List<String> li) { 
            mContext = c; 
            lis = li; 
            TypedArray a = obtainStyledAttributes(R.styleable.Gallery); 
            mGalleryItemBackground = a.getResourceId(R.styleable.Gallery_android_galleryItemBackground, 0); 
            a.recycle(); 
        } 
 
        /* 几定要重写的方法getCount,传回图片数目 */ 
        public int getCount() { 
            return lis.size(); 
        } 
 
        /* 一定要重写的方法getItem,传回position */ 
        public Object getItem(int position) { 
            return lis.get(position); 
        } 
 
        /* 一定要重写的方法getItemId,传并position */ 
        public long getItemId(int position) { 
            return position; 
        } 
         
        /* 几定要重写的方法getView,传并几View对象 */ 
        public View getView(int position, View convertView, ViewGroup parent) { 
            System.out.println("lis:"+lis); 
            File file = new File(it.get(position)); 
            SoftReference<Bitmap> srf = imageCache.get(file.getName()); 
            Bitmap bit = srf.get(); 
            ImageView i = new ImageView(mContext); 
            i.setImageBitmap(bit); 
            i.setScaleType(ImageView.ScaleType.FIT_XY); 
            i.setLayoutParams( new Gallery.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT, 
                    WindowManager.LayoutParams.WRAP_CONTENT)); 
            return i; 
        } 
    } 
}
