package nmg.demo.activity;

import java.io.Serializable;
import java.util.List;
import nmg.online.model.NewsData;
import nmg.online.model.NewsDataBody;
import nmg.online.ui.IJsonAdapter;
import nmg.online.util.NMGDataLoadingTask;
import nmg.online.util.NMGPublicData;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class LoadingTaskActivity extends Activity implements IJsonAdapter{

	private Button dataload;
	private ProgressBar progressBar;
	private TextView news;
	private NMGDataLoadingTask dataLoadingTask;
	private String params = "action,type,pagesize,page,category,device_platform,device_version,device_name";
	private String values;
	private int category = 0;
	private int page = 1; 
	private int tpp = 10; 
	
	public static final String URL = "http://www.nmplus.hk/mapi/nmplive/v01.50a/news.php?";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loadingtask_layout);
        dataLoadingTask = new NMGDataLoadingTask(this, NewsDataBody.class);
        dataload = (Button)findViewById(R.id.dataload);
        progressBar = (ProgressBar)findViewById(R.id.progress_bar);
        news = (TextView)findViewById(R.id.news);
        values = "getNewsList,3," + tpp + "," + page + "," + category
		+ ",android," + NMGPublicData.mOSVersion + ","
		+ NMGPublicData.mModel;
        dataload.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				if (dataLoadingTask != null){
//					dataLoadingTask.cancel(true);
//				}
					
				progressBar.setVisibility(View.VISIBLE);
				dataLoadingTask.execute(URL,params,values);
			}
		});
    }

	@Override
	public void onCancelled() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFinished(AsyncTask<?, ?, ?> source, Serializable datas) {
		// TODO Auto-generated method stub
		System.out.println("datas==="+datas);
		NewsDataBody body = (NewsDataBody) datas;
		List<NewsData> newsList = body.data;
		System.out.println("newsList==="+newsList.get(0).news_title);
		int i = 0;
		StringBuffer stringBuffer = new StringBuffer();
		while(i<10){
			stringBuffer.append(newsList.get(i).news_title+"    "+newsList.get(i).news_date2+"\n\n");
			i++;
		}
		progressBar.setVisibility(View.GONE);
		news.setVisibility(View.VISIBLE);
		news.setText(stringBuffer);
	}

	@Override
	public void onBegin() {
		// TODO Auto-generated method stub
		
	}
    
}
