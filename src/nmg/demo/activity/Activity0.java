package nmg.demo.activity;

import java.util.ArrayList;
import java.util.List;
import nmg.online.ui.MyListAdapter;
import nmg.online.util.NMGI18n;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class Activity0 extends Activity implements OnItemClickListener{
	private ListView mListView;
	private List<String> Items;
	private MyListAdapter myListAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		NMGI18n.setI18n(this, NMGI18n.LANG_CONF_FILE); // 從語言配置文件中讀取要設置的語言
		Items = new ArrayList<String>();
		
		Items.add("設備類型參數");
        Items.add("数据解析工具");
        
        myListAdapter = new MyListAdapter(this,Items);
        mListView = (ListView)this.findViewById(R.id.list);
        mListView.setAdapter(myListAdapter);
        mListView.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		switch (position) {
		case 0:
			intent.setClass(Activity0.this, DeviceHelperActivity.class);
			startActivity(intent);
			break;
		case 1:
			intent.setClass(Activity0.this,DataParserActivity.class);
			startActivity(intent);
			break;
		default:
			break;
		}
		
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onPause();
//		System.out.println("feawgartegtey");
//		NMGI18n.setI18n(this, NMGI18n.LANG_CONF_FILE); // 從語言配置文件中讀取要設置的語言
	}
}
