package nmg.demo.activity;

import java.util.ArrayList;
import java.util.List;
import nmg.online.model.Student;
import nmg.online.util.NMGDBHelper;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class DBHelperActivity extends Activity{

	private NMGDBHelper helper;
	private Button insert,query,getCount,delect;
	private TextView content;
	private List<Student> list;
	private String tableName = "texttable";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dbhelper_layout);
		helper = NMGDBHelper.getInstance(this);
		insert = (Button)findViewById(R.id.insert);
		query = (Button)findViewById(R.id.query);
		content = (TextView)findViewById(R.id.content);
		getCount = (Button)findViewById(R.id.getcount);
		delect = (Button)findViewById(R.id.delect);
		list = new ArrayList<Student>();
		list.add(new Student(13 , "a" , 23 , 178.2));
		list.add(new Student(12 , "b" , 23 , 163.6));
		list.add(new Student(11 , "c" , 18 , 196.1));
		insert.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 helper.insert("texttable", list);
			}
		});
		
		query.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				List<Student> students = helper.query(tableName, Student.class);
				String str = null;
				StringBuffer bf = new StringBuffer("");
				for(Student s : students){
					str = "  id="+s.getId()+"  name="+s.getName()+"  age="+s.getAge()+"  heigh="+s.getHeigh();
					bf.append(str+"\n");
				}
				content.setText(bf);
			}
		});
		
		getCount.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int count = helper.getTableCount(tableName);
				System.out.println("getCount==="+ count);
				String s = String.valueOf(count);
				content.setText("记录数=="+s);
			}
		});
		
		delect.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String []s = {"id"} ;
				String []s2 = {"12"};
				helper.delete(tableName, s, s2);
			}
		});
	}

}
