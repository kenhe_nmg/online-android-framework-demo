package nmg.demo.activity;

import nmg.online.util.NMGHttpHelper;
import nmg.online.util.NMGNetworkHelper;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class HttpHelperActivity extends Activity implements OnClickListener{

	private Button getButton;
	private Button postButton;
	private EditText urlText;
	private TextView contentText;
	private TextView network;
	String url;

	String path = "http://www.nmplus.hk/mapi/nmplive/v01.00/news.php?action=getNewsCategory&type=1";
	String params[] = new String[]{"act","category","page","limit"};
	String values[] = new String[]{"getSnapshotList","1","1","5"};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.http_layout);
//		NMGDeviceHelper.getAppInfo(this);

		getButton = (Button)findViewById(R.id.http_get);
		postButton = (Button)findViewById(R.id.http_post);
		urlText = (EditText)findViewById(R.id.url_text);
		contentText = (TextView)findViewById(R.id.content);
		network = (TextView)findViewById(R.id.network);
		
        String netStatic = NMGNetworkHelper.connectModel(this);
		
		StringBuffer sb = new StringBuffer();
		if("false".equals(netStatic))
		{
			sb.append("网络连接失敗！");
		}else
		{
			sb.append("网络连接模式是：").append(netStatic);
		}
		
		
		network.setText(sb.toString());
		urlText.setText(path);
		urlText.clearFocus();
		getButton.setOnClickListener(this);
		postButton.setOnClickListener(this);
		
		//3.0以上版本添加
//		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()       
//        .detectDiskReads()       
//        .detectDiskWrites()       
//        .detectNetwork()          
//        .penaltyLog()       
//        .build());       
//		StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()       
//        .detectLeakedSqlLiteObjects()    
//        .penaltyLog()       
//        .penaltyDeath()       
//        .build()); 
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.http_get:
			url = urlText.getText().toString();
			String str = NMGHttpHelper.getRequestContent(url);
			contentText.setText(str);
			System.out.println("str = " + str);
			break;

		case R.id.http_post:
			url = urlText.getText().toString();
			String content = NMGHttpHelper.post(url, params , values);
			System.out.println("# content = " + content);
			contentText.setText("code = " + NMGHttpHelper.getResponseCode());
			
			break;
		}
	}

	
}
