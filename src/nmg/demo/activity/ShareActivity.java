package nmg.demo.activity;

import nmg.online.util.NMGFacebookSNS;
import nmg.online.util.NMGSinaSNS;
import nmg.online.util.NMGTengxunSNS;
import nmg.online.util.NMGTwitterSNS;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import com.tencent.weibo.oauthv2.OAuthV2;
import com.tencent.weibo.webview.OAuthV2AuthorizeWebView;

public class ShareActivity extends Activity {
	/** Called when the activity is first created. */

	private Button login_fb, share_fb, exit_fb, login_sina, share_sina,twitter_button,share_twitter,
	        logout_twitter,login_tengxun,share_tengxun,layout_tengxun,logout_sina;
	private Context mContext;
	private NMGFacebookSNS fb;
	private NMGSinaSNS sina;
	private NMGTengxunSNS tengxun;
	private NMGTwitterSNS twitterSNS;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.share_layout);
		mContext = this;
		fb = new NMGFacebookSNS(mContext,ShareActivity.this);
		sina = new NMGSinaSNS(mContext, ShareActivity.this);
		tengxun = new NMGTengxunSNS(mContext, ShareActivity.this);
		twitterSNS = new NMGTwitterSNS(mContext, ShareActivity.this);

		fb.isLogin();
		sina.isLogin();
		tengxun.isLogin();
		twitterSNS.isLogin();

		login_fb = (Button) findViewById(R.id.login_fb);
		share_fb = (Button) findViewById(R.id.share_fb);
		exit_fb = (Button) findViewById(R.id.exit_fb);
		login_sina = (Button) findViewById(R.id.login_sina);
		share_sina = (Button) findViewById(R.id.share_sina);
		logout_sina = (Button) findViewById(R.id.logout_sina);
		twitter_button = (Button)findViewById(R.id.login_twitter);
		share_twitter = (Button)findViewById(R.id.share_twitter);
		logout_twitter = (Button)findViewById(R.id.logout_twitter);
		login_tengxun = (Button)findViewById(R.id.tengxun_login);
		share_tengxun = (Button)findViewById(R.id.tengxun_share);
		layout_tengxun = (Button)findViewById(R.id.tengxun_layout);

		login_fb.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				fb.snsLogin();

			}
		});

		share_fb.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Bundle params = new Bundle();
				params.putString("link",
						"http://www.nmplus.hk/mobile/live/news.php?id=3");
				params.putString("name", "hellow");
				params.putString("description", "165464549849498");
				fb.snsShare(params);
			}
		});

		exit_fb.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				fb.snsLogout();
			}
		});

		login_sina.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				sina.snsLogin();
			}
		});

		share_sina.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String content = "fegasdfregawerfea";
				sina.snsShare(content,null);

			}
		});

		logout_sina.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				sina.snsLogout();
			}
		});
		
		twitter_button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				twitterSNS.snsLogin();
			}
		});
		
		share_twitter.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String s = "fjoiarhgonvoiag";
				twitterSNS.snsShare(s);
			}
		});
		
		logout_twitter.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				twitterSNS.snsLogout();
			}
		});
		
		login_tengxun.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				tengxun.snsLogin();
			}
		});
		
		share_tengxun.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				tengxun.snsShare();
			}
		});
		
		layout_tengxun.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				tengxun.snsLogout();
			}
		});

	}

	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		NMGFacebookSNS.myFacebook.authorizeCallback(requestCode, resultCode, data);
		if (requestCode==2) {
            if (resultCode==OAuthV2AuthorizeWebView.RESULT_CODE)    {
                
            	tengxun.oAuth = (OAuthV2) data.getExtras().getSerializable("oauth");
            	tengxun.snsResult();
                if(tengxun.oAuth.getStatus()==0)
                    Toast.makeText(getApplicationContext(), "腾讯微博登陆成功", Toast.LENGTH_SHORT).show();
            }
        }
	}

}
