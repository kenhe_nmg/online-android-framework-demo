package nmg.demo.activity;

import nmg.online.util.NMGReadBitMap;
import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;

public class ReadBitmapActivity extends Activity {
	ImageView imageView;
	Bitmap bitmap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.read_bitmap);
		imageView = (ImageView) findViewById(R.id.bitmap_image);
		bitmap = NMGReadBitMap.readBitMap(this, R.drawable.bitmap_icon);
		imageView.setImageBitmap(bitmap);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (bitmap != null && !bitmap.isRecycled()) {
			bitmap.recycle(); // 回收图片所占的内存
			System.gc(); // 提醒系统及时回收
			System.out.println("bitmap=="+bitmap+"===onDestroy");
		}
	}
}
