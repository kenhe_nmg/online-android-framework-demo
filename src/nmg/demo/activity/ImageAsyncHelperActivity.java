package nmg.demo.activity;

import java.io.File;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

import nmg.online.util.NMGDownloadAsyncHelper;
import nmg.online.util.NMGDownloadAsyncHelperInfo;
import nmg.online.util.NMGFileHelper;
import nmg.online.util.NMGHttpHelper;
import nmg.online.util.NMGPublicData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

/**
 * @Title: ImageAsyncHelperActivity.java 
 * @Description: 图片异步下载 
 * @author jason
 *
 */
public class ImageAsyncHelperActivity extends Activity {

	private ListView mListView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.download_asyn_helper_view);
		mListView = (ListView) this.findViewById(R.id.listview);

		String json = NMGHttpHelper.getRequestContent("http://www.nmplus.hk/mapi/nmplive/v01.50/news.php?action=getNewsDetail&news_id=2703&nm_token=6eq7iwfm9a&nm_sign=1");
		System.out.println("json====" + json);
		List<String> mList = new ArrayList<String>();
		try {
			JSONObject jsonObject = new JSONObject(json);
			JSONObject dataObject = jsonObject.getJSONObject("data");
			JSONArray jsonArray = dataObject.getJSONArray("newsphoto_list");
			for (int i = 0; i < jsonArray.length(); i++) {
				String string = (String) jsonArray.getJSONArray(i).get(2);
				mList.add(string);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		MyAdapter adapter = new MyAdapter(this, mList);
		mListView.setAdapter(adapter);
	}

	class MyAdapter extends BaseAdapter {

		List<String> mList;
		Context mContext;
		private LayoutInflater inflater;

		public MyAdapter(Context mContext, List<String> mList) {
			this.mContext = mContext;
			this.mList = mList;
			inflater = LayoutInflater.from(mContext);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return mList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Holder holder = null;
			if (convertView == null) {
				holder = new Holder();
				convertView = inflater.inflate(R.layout.async_imag, null);
				holder.mImageView = (ImageView) convertView
						.findViewById(R.id.image_item);
				convertView.setTag(holder);
			} else {
				holder = (Holder) convertView.getTag();
			}

			String imgURL = mList.get(position);
			System.out.println("imgURL = " + imgURL);
			if (imgURL != null && !"".equals(imgURL.trim())) {
				// 先初始化ImageView的图片
				String imgName = imgURL.substring(imgURL.lastIndexOf('/') + 1,imgURL.length());
				System.out.println("imgName1=="+imgName);
				imgName = imgName.substring(0, imgName.lastIndexOf("."))+ ".jpg";
				System.out.println("imgName2=="+imgName);
				Drawable d = null ;
				try {
					File file = new File(NMGFileHelper.returnAbsFilePath(mContext, NMGPublicData.IMAGE_DIR +imgName, 1));
					if (file.exists()) {
						d = Drawable.createFromPath(file.getAbsolutePath());
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println("d===="+d);
				// 判断localApp的缓存图片是否存在
				if (d!= null) {
					SoftReference<Drawable> softRef = new SoftReference<Drawable>(d);
					// 缓存图片存在，且与默认图片不一样时，把缓存图片设为ImageView的图片
					NMGDownloadAsyncHelper.clearViewMap(holder.mImageView);
					holder.mImageView.setImageDrawable(softRef.get());
					d = null;
				} else {
					System.out.println("# -----------------");
					// 异步下载图片，并写入SDCard
					new NMGDownloadAsyncHelper().execute(new NMGDownloadAsyncHelperInfo(mList.get(position), holder.mImageView, mContext));
					System.out.println("mList.get(position)==="+mList.get(position));
				}
			}
			return convertView;
		}
		class Holder {
			private ImageView mImageView;
		}

	}

}
