package nmg.demo.activity;

import java.io.File;
import java.io.InputStream;
import nmg.online.util.NMGFileHelper;
import nmg.online.util.NMGHttpHelper;
import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class FileHelperActivity extends Activity implements
		View.OnClickListener {
	// private Intent intent;
	private static final String TAG = "DemoFileHelper";

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.file_layout);

		Log.i(TAG,
				"程序数据目录：" + getFilesDir() + " \n SDCard目录路径："
						+ Environment.getExternalStorageDirectory()
						+ "\n 缓存的路径：" + getCacheDir()
						+ "\n getExternalCacheDir()=" + getExternalCacheDir()
						+ " \n getExternalFilesDir('abcd')="
						+ getExternalFilesDir("abcd")
						+ " \n getDatabasePath('tempdb')="
						+ getDatabasePath("tempdb"));
		init();
	}

	private void init() {
		// TextView textView = (TextView) findViewById(R.id.textView);

		Button buttonDelFiles = (Button) findViewById(R.id.buttonDelFiles);
		Button buttonSDCardDir = (Button) findViewById(R.id.buttonSDCardDir);
		Button buttonFileExist = (Button) findViewById(R.id.buttonFileExist);
		Button buttonSaveFile = (Button) findViewById(R.id.buttonSaveFile);
		Button buttonCopyFile = (Button) findViewById(R.id.buttonCopyFile);
		Button buttonZip = (Button) findViewById(R.id.buttonZip);
		Button buttonUzip = (Button) findViewById(R.id.buttonUzip);
		Button buttonLen = (Button) findViewById(R.id.buttonLen);

		buttonDelFiles.setOnClickListener(this);
		buttonSDCardDir.setOnClickListener(this);
		buttonFileExist.setOnClickListener(this);
		buttonSaveFile.setOnClickListener(this);
		buttonCopyFile.setOnClickListener(this);
		buttonZip.setOnClickListener(this);
		buttonUzip.setOnClickListener(this);
		buttonLen.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.buttonDelFiles: // 删除缓存目录下的所有文件及文件夹
			String cacheDir = this.getCacheDir().getAbsolutePath();
			NMGFileHelper.deleteFiles(cacheDir, true);
			Toast.makeText(this, "删除缓存目录下的所有文件及文件夹，操作已经完成:" + cacheDir,Toast.LENGTH_SHORT).show();
			break;

		case R.id.buttonSDCardDir: // 根据给定的相对路径名，创建sdcard里面的目录：
			String dir = "myDir/a/b/c";
			String str = NMGFileHelper.createSDCardDir(dir);
			if (str.equals("noSDCard")) {
				Toast.makeText(this, "对不起，你的设备检测不到SDCard", Toast.LENGTH_SHORT).show();
			} else if (str.equals("false")) {
				Toast.makeText(this, "对不起，请检查你给的相对路径是否合法：" + dir,Toast.LENGTH_SHORT).show();
			} else if (str.equals("true")) {
				Toast.makeText(this, "你给的文件夹已经存在sdcard中:" + str,Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(this, "文件夹创建成功，完整路径是：" + str, Toast.LENGTH_SHORT).show();
			}
			break;

		case R.id.buttonFileExist: // 检测是否存在此文件： 1:是检测sdcard
			String dir3 = "myDir";
			if (NMGFileHelper.isFileExist(this, dir3, 1)) {
				Toast.makeText(this, "检测sdcard里面存在此文件：" + dir3,Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(this, "不存在此文件：" + dir3, Toast.LENGTH_SHORT).show();
			}
			break;

		case R.id.buttonSaveFile: // 把输入流（或二进制）保存到SDCard中
			String dir4 = "a";
			String filename = "file1.txt";
			String str4 = null;

			// 传输输入流进行文件保存
			InputStream is = null;
			try {
				is = NMGHttpHelper.getConn(("http://emperorgroup.nmg.com.hk/mapi/get_sqlite_db.php")).getInputStream();
				// is = HttpUtil.getRequest(
				// "http://emperorgroup.nmg.com.hk/mapi/get_sqlite_db.php");
				// is = new FileInputStream("/mnt/sdcard/createfile.txt");

				str4 = NMGFileHelper.createFile(this, is, dir4, filename, 1);
			} catch (Exception e) {
				Log.e(TAG, e.toString());
			} finally {
				if (is != null) {
					try {
						is.close();
					} catch (Exception e) {
						Log.e(TAG, e.toString());
					}
				}
			}

			// 将二进制字节进行文件保存
			// byte[] bytes =
			// NMGHttpHelper.getBytes("http://emperorgroup.nmg.com.hk/mapi/get_sqlite_db.php");
			// Log.i(TAG,"bytes长度=" + bytes.length);
			// String str4 = NMGFileHelper.createFile(this, bytes, dir4,
			// filename, 1);

			if (!str4.equals("false")) {
				Toast.makeText(this, "保存文件到SDCard中成功：" + str4,Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(this, "保存文件到SDCard中失败：" + str4,Toast.LENGTH_SHORT).show();
			}

			/*
			 * if(is != null) { try { is.close(); } catch (IOException e) {
			 * e.printStackTrace(); } }
			 */

			break;

		case R.id.buttonCopyFile: // 复制文件
			File srcFile = new File("/mnt/sdcard/a/file1.txt");
			File desFile = new File("/mnt/sdcard/createfile2.txt");
			if (NMGFileHelper.copyFile(srcFile, desFile)) {
				Toast.makeText(this, "复制文件成功：" + srcFile.getAbsolutePath(),Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(this, "复制文件失败：" + srcFile.getAbsolutePath(),Toast.LENGTH_SHORT).show();
			}
			;
			break;

		case R.id.buttonZip: // 压缩文件

			boolean b = NMGFileHelper.zipFile("/mnt/sdcard/a/file1.txt","/mnt/sdcard/file1zip.zip");
			if (b) {
				Toast.makeText(this,"压缩文件成功："+ "/mnt/sdcard/a/file1.txt 压缩成   /mnt/sdcard/file1zip.zip",Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(this,"压缩文件失败："+ "/mnt/sdcard/a/file1.txt 压缩成   /mnt/sdcard/file1zip.zip",Toast.LENGTH_SHORT).show();
			}
			break;

		case R.id.buttonUzip: // 解压缩文件
			boolean b2 = NMGFileHelper.uzipFile("/mnt/sdcard/abc.zip","/mnt/sdcard/a/abc.sqlite");
			if (b2) {
				Toast.makeText(this,"解压缩文件成功："+ "/mnt/sdcard/abc.zip 解压缩成  /mnt/sdcard/a/abc.sqlite ",Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(this,"解压缩文件失败："+ "/mnt/sdcard/abc.zip 解压缩成 /mnt/sdcard/a/abc.sqlite ",Toast.LENGTH_SHORT).show();
			}
			break;

		case R.id.buttonLen:
			long i = NMGFileHelper.fileLength("/mnt/sdcard/a/file1.txt");
			if(i!=-1){
				String filelength = NMGFileHelper.FormetFileSize(i);
				Toast.makeText(this,"文件大小=="+filelength,Toast.LENGTH_SHORT).show();
			}else{
				Toast.makeText(this,"文件不存在",Toast.LENGTH_SHORT).show();
			}
			
		}
	}

}
