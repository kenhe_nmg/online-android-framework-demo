package nmg.demo.activity;

import java.util.List;
import nmg.online.util.NMGViewPager;
import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ViewActivity extends Activity{
	
	private TextView textView1,textView2,textView3;
	private ViewPager mPager;
	private ImageView cursor;
	private NMGViewPager nmgViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.viewpager_layout);
		textView1 = (TextView)findViewById(R.id.text1);
		textView2 = (TextView)findViewById(R.id.text2);
		textView3 = (TextView)findViewById(R.id.text3);
		cursor = (ImageView)findViewById(R.id.cursor);
		mPager = (ViewPager)findViewById(R.id.vPager);
		nmgViewPager = new NMGViewPager(ViewActivity.this, cursor, mPager);
		nmgViewPager.InitImageView(R.drawable.viewcouser);
		nmgViewPager.InitViewPager();
		textView1.setOnClickListener(nmgViewPager.new MyOnClickListener(0));
		textView2.setOnClickListener(nmgViewPager.new MyOnClickListener(1));
		textView3.setOnClickListener(nmgViewPager.new MyOnClickListener(2));
	}
	
	/**
	 * ViewPager适配器
	 */
	public static class MyPagerAdapter extends PagerAdapter {
		public List<View> mListViews;

		public MyPagerAdapter(List<View> mListViews) {
			this.mListViews = mListViews;
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView(mListViews.get(arg1));
		}

		@Override
		public void finishUpdate(View arg0) {
		}

		@Override
		public int getCount() {
			return mListViews.size();
		}

		//此处添加各个view的控件事件等 
		@Override
		public Object instantiateItem(View arg0, int arg1) {
			((ViewPager) arg0).addView(mListViews.get(arg1), 0);
			if(arg1 == 0){
				Button but = (Button)arg0.findViewById(R.id.view1button);
				final TextView textView = (TextView)arg0.findViewById(R.id.view1text);
				but.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						textView.setText("-----这是页卡1-----");
					}
				});
			}
			return mListViews.get(arg1);
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == (arg1);
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View arg0) {
		}
	}
	
}
