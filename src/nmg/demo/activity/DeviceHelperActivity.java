package nmg.demo.activity;

import nmg.online.util.NMGDeviceHelper;
import nmg.online.util.NMGPublicData;
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * @Title: DeviceHelperActivity.java 
 * @Description: 设备参数 
 * @author jason
 *
 */
public class DeviceHelperActivity extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.demo_layout);
		init();
	}
	private void init(){
		TextView textView = (TextView) findViewById(R.id.textView);
		NMGDeviceHelper.getResolution(this);
		NMGDeviceHelper.getAppInfo(this);
		StringBuffer sbBuffer = new StringBuffer();
		sbBuffer.append(" 屏幕宽：").append(NMGPublicData.mScreenWidth)
		.append("\n 屏幕高：").append(NMGPublicData.mScreenHeight)
		.append("\n 設備ID:").append(NMGPublicData.mDeviceUID)
		.append("\n 藍牙裡面定义的机器名称:").append(NMGPublicData.mBluetoothDeviceName)
		.append("\n 程序的版本号:").append(NMGPublicData.mAppVersion)
		.append("\n 程序的版本代码号:").append(NMGPublicData.mVersionCode)
		.append("\n 程序的完整包名:").append(NMGPublicData.mPackageName) 
		.append("\n 程序的名称:").append(NMGPublicData.mAppName)
		.append("\n 机器型号:").append(NMGPublicData.mModel)
		.append("\n sdk的开发版本号:").append(NMGPublicData.mSDKVersion)
		.append("\n 操作系统版本号:").append(NMGPublicData.mOSVersion)
		.append("\n SD卡总容量，单位Mb:").append(NMGPublicData.mSDTotalSize)
		.append("\n SD卡剩余容量，单位Mb:").append(NMGPublicData.mSDFreeSize)
		.append("\n 磁盘总容量，单位Mb:").append(NMGPublicData.mTotalSize)
		.append("\n 磁盘剩余容量，单位Mb:").append(NMGPublicData.mFreeSize)
		.append("\n 可用内存，单位Mb:").append(NMGPublicData.mMemoFreeSize)
		.append("\n Mac地址:").append(NMGPublicData.mMacAddress)
		.append("\n IP地址:").append(NMGPublicData. mIPAddress)		
		.append("\n ").append("");
		
		textView.setText(sbBuffer.toString());
	}
}
