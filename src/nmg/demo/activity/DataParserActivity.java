package nmg.demo.activity;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import nmg.online.model.Body;
import nmg.online.model.Person;
import nmg.online.util.NMGDataParser;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * @Title: DataParserActivity.java 
 * @Description: 数据解析 
 * @author jason
 *
 */
public class DataParserActivity extends Activity implements OnClickListener {
	private Button xmlButton;
	private Button jsonButton;
	private TextView contentText;
	private static final String JSON_ARRAY = "{\"body\":[{\"name\":\"Tom\",\"age\":\"35\",\"heigh\":\"173\"},{\"name\":\"hgk\",\"age\":\"18\",\"heigh\":\"180\"}]}";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.data_parser_view);
		xmlButton = (Button) this.findViewById(R.id.xml_parser);
		jsonButton = (Button) this.findViewById(R.id.json_parser);
		contentText = (TextView) this.findViewById(R.id.content);
		xmlButton.setOnClickListener(this);
		jsonButton.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.xml_parser:
			try {
				Person person = new Person();
				InputStream inputStream = this.getAssets().open("person.xml");
				InputStream inputStream2 = this.getAssets().open("person.xml");
				byte[] buf = new byte[1024];
				int len = 0;
				StringBuilder sb = new StringBuilder();
				while ((len = inputStream2.read(buf))!=-1) {
					sb.append(new String(buf , 0 , len));
				}
				sb.append("\n");
				List<Person> persons = NMGDataParser.getNetworkData(inputStream, person, 0);
				for(Person p : persons)
				{
					sb.append("name = ").append(p.getName())
					.append(" , age = ").append(p.getAge())
					.append(" , heigh = ").append(p.getHeigh())
					.append("\n");
				}
				System.out.println(sb.toString());
				contentText.setText(sb.toString());
			}catch (FileNotFoundException e) {
				// TODO: handle exception
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case R.id.json_parser:
			Body person = new Body();
			StringBuilder sb = new StringBuilder();
			sb.append(JSON_ARRAY).append("\n");
			InputStream in = new ByteArrayInputStream(JSON_ARRAY.getBytes());
			List<Body> body = NMGDataParser.getNetworkData(in, person , 1);
			for(Person p : body.get(0).body)
			{
				sb.append("name = ").append(p.getName())
				.append(" , age = ").append(p.getAge())
				.append(" , heigh = ").append(p.getHeigh())
				.append("\n");
			}
			System.out.println(sb.toString());
			contentText.setText(sb.toString());
			break;
		default:
			break;
		}
	}
}
