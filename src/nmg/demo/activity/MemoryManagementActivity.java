package nmg.demo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * @Title: MemoryManagementActivity.java 
 * @Description: 解决图片内存溢出 
 * @author jason
 *
 */
public class MemoryManagementActivity extends Activity implements
		OnClickListener {
	Button readbitmap,phtoButton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.memory_management);
		readbitmap = (Button) findViewById(R.id.read_bitmap_btn);
		phtoButton = (Button)findViewById(R.id.read_photo);
		phtoButton.setOnClickListener(this);
		readbitmap.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		switch (v.getId()) {
		case R.id.read_bitmap_btn:
			intent.setClass(this, ReadBitmapActivity.class);
			startActivity(intent);
			break;
		case R.id.read_photo:
			intent.setClass(this, PhotoScanActivity.class);
			startActivity(intent);
			break;
		default:
			break;
		}
	}
}
