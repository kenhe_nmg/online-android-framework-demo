package nmg.demo.activity;

import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.widget.FrameLayout;
import com.example.mylineartoolbar.customerview.HeaderMenu;
import com.example.mylineartoolbar.customerview.OnTouchListener;
import com.example.mylineartoolbar.customerview.ToolbarAItem;

public class MainActivity extends ActivityGroup implements OnTouchListener {
	
	private HeaderMenu mHeaderMenu = null;
	private FrameLayout layout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        layout = (FrameLayout)findViewById(R.id.framelayout);
        mHeaderMenu = (HeaderMenu) findViewById(R.id.appHeaderMenu);
		// 添加头部 菜单项目

		mHeaderMenu.addItem(new ToolbarAItem(mHeaderMenu,
				R.drawable.tabbar_icon_1,
				R.drawable.tabbar_icon_1_s,
				R.string.classification1, 
				"1", 1.0f));
		mHeaderMenu.addItem(new ToolbarAItem(mHeaderMenu,
				R.drawable.tabbar_icon_2,
				R.drawable.tabbar_icon_2_s,
				R.string.classification2,
				null , 1.0f));
		mHeaderMenu.addItem(new ToolbarAItem(mHeaderMenu,
				R.drawable.tabbar_icon_3,
				R.drawable.tabbar_icon_3_s,
				R.string.classification3,
				"3", 1.0f));
		mHeaderMenu.addItem(new ToolbarAItem(mHeaderMenu,
				R.drawable.tabbar_icon_4,
				R.drawable.tabbar_icon_4_s,
				R.string.classification4,
				"4", 1.0f));
		mHeaderMenu.addItem(new ToolbarAItem(mHeaderMenu,
				R.drawable.tabbar_icon_5,
				R.drawable.tabbar_icon_5_s,
				R.string.classification5,
				"5", 1.0f));
		// 设置头部菜单的ItemOnTouchListener事件
		mHeaderMenu.setOnItemClickListener(this);
		// 初始化菜单
		mHeaderMenu.performItemClick(0);
    }

	
	@Override
	public void onItemTouched(int position, String name) {
		// TODO Auto-generated method stub
		layout.removeAllViews();
		Intent intent = new Intent();
		switch (position) {
		case 0:
			System.out.println("00000");
			intent.setClass(this, Activity0.class);
			layout.addView(getLocalActivityManager().startActivity("Activity0",intent.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY)).getDecorView());
			break;
		case 1:
			intent.setClass(this, Activity1.class);
			layout.addView(getLocalActivityManager().startActivity("Activity1",intent.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY)).getDecorView());
			System.out.println("11111");
			break;
		case 2:
			intent.setClass(this, Activity2.class);
			layout.addView(getLocalActivityManager().startActivity("Activity2",intent.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY)).getDecorView());
			System.out.println("22222");
			break;
		case 3:
			intent.setClass(this, Activity3.class);
			layout.addView(getLocalActivityManager().startActivity("Activity3",intent.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY)).getDecorView());
			System.out.println("33333");
			break;
		case 4:
			intent.setClass(this, Activity4.class);
			layout.addView(getLocalActivityManager().startActivity("Activity4",intent.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY)).getDecorView());
			System.out.println("44444");
			break;
		default:
			break;
		}
		
	}
}
