package nmg.demo.activity;


import nmg.online.util.NMGI18n;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class I18nActivity extends Activity implements OnClickListener{

	private Button buttonTW,buttonCN,buttonENG;
	private Intent intent;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		NMGI18n.setI18n(this, NMGI18n.LANG_CONF_FILE); // 從語言配置文件中讀取要設置的語言
		setContentView(R.layout.i18n_layout);
		buttonTW = (Button) findViewById(R.id.buttonTW);
		buttonCN = (Button) findViewById(R.id.buttonCN);
		buttonENG = (Button) findViewById(R.id.buttonENG);
		buttonTW.setOnClickListener(this);
		buttonCN.setOnClickListener(this);
		buttonENG.setOnClickListener(this);
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.buttonTW:	
			NMGI18n.saveLangConf(I18nActivity.this, NMGI18n.LANG_CONF_FILE, NMGI18n.BIG5);
			intent = new Intent(this, I18nActivity.class);		
			startActivity(intent);
			finish();
			break;
		case R.id.buttonCN:	
			NMGI18n.saveLangConf(I18nActivity.this, NMGI18n.LANG_CONF_FILE, NMGI18n.GB);
			intent = new Intent(this, I18nActivity.class);		
			startActivity(intent);
			finish();
			break;
		case R.id.buttonENG:	
			NMGI18n.saveLangConf(I18nActivity.this, NMGI18n.LANG_CONF_FILE, NMGI18n.ENG);
			intent = new Intent(this, I18nActivity.class);		
			startActivity(intent);
			finish();
			break;
		}
	}
}
