package com.example.mylineartoolbar.customerview;

import android.view.View;

public abstract class ToolbarItem {	
	protected HeaderMenu mParent;
	protected float mWeight;

	public ToolbarItem(HeaderMenu parent, float weight) {
		mParent = parent;
		mWeight = weight;
	}
	
	public abstract View getView();
	public abstract void setEnabled(boolean enabled);
	
	protected int dipToPixels(int dipper) {
		return ((int) (dipper * mParent.getContext().getResources().getDisplayMetrics().density));
	}
}
