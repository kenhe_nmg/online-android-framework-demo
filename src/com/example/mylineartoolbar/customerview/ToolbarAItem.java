package com.example.mylineartoolbar.customerview;

import com.weibo.android.R;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;


public class ToolbarAItem extends ToolbarClickableItem {

	private int textColor = 0xFF000000;				// 字体颜色
	private int textFocusColor = 0xFF000000;		// 字体颜色
	
	public int mImage;
	public int mFocusImage;
	public int mText;
	
	private View mainView;
	private ImageView imageView;
	private TextView textView;
//	private TextView badgeView;

	
	/*初始化菜单项目*/
	public ToolbarAItem(HeaderMenu parent, int image_resid, int focusimage_resid, int text_resid, String badge, float weight) {
		super(parent, weight);
		mFocusImage = focusimage_resid;
		mImage = image_resid;
		mText = text_resid;
		
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		
		View view = inflater.inflate(R.layout.menubar_item, null);
		imageView = (ImageView)view.findViewById(R.id.itemIcon);
		textView = (TextView)view.findViewById(R.id.itemText);
//		badgeView = (TextView) view.findViewById(R.id.itemBadge);
		
		view.setClickable(true);
		view.setFocusable(true);
		
		view.setOnFocusChangeListener(this);
		view.setOnTouchListener(this);
		view.setOnClickListener(this);
		
		imageView.setImageResource(mImage);
		textView.setText(mText);
//		if (badge != null) {
//			badgeView.setText(badge);
//			badgeView.setVisibility(View.VISIBLE);
//		}
		mainView = view;
	}
	
	/**
	 * 设置字体颜色
	 * @param textColor			普通状态下颜色
	 * @param textFocusColor	选中状态下颜色
	 */
	public void setTextColor(int textColor, int textFocusColor){
		this.textColor = textColor;
		this.textFocusColor = textFocusColor;
	}
	
	/**
	 * 设置badge文本值
	 * @param badge
	 */
//	public void setBadgeValue(String badge){
//		if (badge != null) {
//			this.badgeView.setText(badge);
//			this.badgeView.setVisibility(View.VISIBLE);
//		}else {
//			this.badgeView.setVisibility(View.GONE);
//		}
//	}
	

	/**
	 * 取消选中状态时处理
	 */
	@Override
	public void unFocus(){
		textView.setTextColor(textColor);
		imageView.setImageResource(mImage);
		mainView.setBackgroundResource(mParent.getDefaultBGDrawableId());
	}
	
	/**
	 * 选中状态时处理
	 */
	@Override
	public void onFocus(){
		textView.setTextColor(textFocusColor);
		imageView.setImageResource(mFocusImage);
		mainView.setBackgroundResource(mParent.getSelectBGDrawableId());
	}
	
	/**
	 * 设置Item可用性
	 */
	public void setEnabled(boolean enabled) {
		mainView.setEnabled(enabled);
		/*过滤颜色, 默认可用:白色, 不可用:灰色*/
		if (enabled) {
			textView.setTextColor(textColor);
			imageView.setColorFilter(null);
		} else {
			textView.setTextColor(mParent.getTextDisableColor());
			PorterDuffColorFilter tintGreen = new PorterDuffColorFilter(
					   0xEE888888, PorterDuff.Mode.SRC_ATOP);
			imageView.setColorFilter(tintGreen);
		}
	}

	@Override
	public View getView() {
		if (mWeight > 0) {
			mainView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, mWeight));
		} else {
			mainView.setLayoutParams(new LayoutParams(dipToPixels(mParent.getItemWidth()), LayoutParams.FILL_PARENT));
		}
		return mainView;
	}
}
