package com.example.mylineartoolbar.customerview;

import android.view.MotionEvent;
import android.view.View;

public abstract class ToolbarClickableItem extends ToolbarItem
	implements View.OnClickListener, 
		View.OnTouchListener,
		View.OnFocusChangeListener {
	boolean mReclickable = false;
	
	public ToolbarClickableItem(HeaderMenu parent, float weight) {
		super(parent, weight);
	}

	@Override
	public void onClick(View v) {
		int index = mParent.getIndexOf(this);
		if (mReclickable || index != mParent.getPosition()) {
			mParent.performItemClick(this);
		}
	}
	
	public void setReclickable(boolean value) {
		mReclickable = value;
	}
	
	/**
	 * 触模点击事件
	 */
	private boolean out = true;
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int index = mParent.getIndexOf(this);
		if (index != mParent.getPosition()) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
//				OnTuchEvenEffect.OnTuchEven(mParent.getContext());
				out = false;
				onFocus();
				//mParent.invalidate();
			} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
				if (event.getX() < 0 || event.getY() < 0 || 
						event.getX() > v.getWidth() || event.getY() > v.getHeight()) {
					if (! out) {
						unFocus();
						out = true;
					}
				} else {
					if (out) {
						onFocus();
						out = false;
					}
				}
				//mParent.invalidate();
			} 
		} 
		return false;
	}
	

	/**
	 * 焦点改变事件
	 */
	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		int index = mParent.getIndexOf(this);
		if (hasFocus) {
			onFocus();
		} else if (mParent.getPosition() != index) {
			unFocus();
		}
	}
	
	public void unFocus() {
		
	};
	public void onFocus() {
		
	}
}
