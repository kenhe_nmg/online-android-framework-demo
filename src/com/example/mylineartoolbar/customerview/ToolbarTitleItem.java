package com.example.mylineartoolbar.customerview;

import nmg.demo.activity.R;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

public class ToolbarTitleItem extends ToolbarItem {
	
	public int mText;
	public int mTextColor;
	public String mTextString;
	private View mainView;
	private TextView textView;
	
	public ToolbarTitleItem(HeaderMenu parent, int text_resid, int text_color, float weight) {
		super(parent, weight);
		mText = text_resid;
		mTextColor = text_color;
		
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		
		View view = inflater.inflate(R.layout.menubar_titleitem, null);
		textView = (TextView)view.findViewById(R.id.itemText);
		textView.setText(mText);
		textView.setTextColor(mTextColor);
		mainView = view;
	}
	
	public ToolbarTitleItem(HeaderMenu parent, String text, int text_color, float weight) {
		super(parent, weight);
		mTextString = text;
		mTextColor = text_color;
		
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		
		View view = inflater.inflate(R.layout.menubar_titleitem, null);
		textView = (TextView)view.findViewById(R.id.itemText);
		textView.setText(mTextString);
		textView.setTextColor(mTextColor);
		mainView = view;
	}

	@Override
	public View getView() {
		if (mWeight > 0) {
			mainView.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT, mWeight));
		} else {
			mainView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT));
		}
		return mainView;
	}

	@Override
	public void setEnabled(boolean enabled) {
		mainView.setEnabled(enabled);
	}

}
