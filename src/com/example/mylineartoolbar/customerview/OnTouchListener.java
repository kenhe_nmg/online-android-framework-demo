package com.example.mylineartoolbar.customerview;

/**
 * UI控件触摸监听接口
 */
public interface OnTouchListener {
	
	public void onItemTouched(int position,String name);
	
}
