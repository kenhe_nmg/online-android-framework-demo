package com.example.mylineartoolbar.customerview;

import java.util.ArrayList;

import nmg.demo.activity.R;
import nmg.online.util.Logger;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * 下部工具栏
 */
public class HeaderMenu extends LinearLayout {	
	private static final String LOG_TAG = "ToolbarView"; 
	
	//public interface OnItemClickListener {
	//	public void OnItemClick(View view, int position);
	//} 
	
	private int position = 0;											// 当前选中
	private ArrayList<ToolbarItem> items;								// 菜单列表
	private com.example.mylineartoolbar.customerview.OnTouchListener listener;		// 点击事件
	
	private int textColor, textFocusColor;								// 字体颜色
	private int textDisableColor;										// 不可用时字体颜色
	private int drawableSelected;										// 选中背景
	private int drawableSplit;											// 分隔栏图片
	private int splitWidth = 0;											// 分隔栏宽度
	private int itemWidth = 0;											// 设置每项宽度
	
	/*构造函数*/
	public HeaderMenu(Context context) {
		super(context);
		init();
	}
	
	/*构造函数*/
	public HeaderMenu(Context context, AttributeSet attrs) {
		super(context, attrs);
		loadAttribute(context, attrs);
		init();
	}
	
	/*初始化*/
	private void init(){
		items = new ArrayList<ToolbarItem>();
	}
	
	/**
	 * 读取 属性设置值
	 * @param context
	 * @param attrs
	 */
	private void loadAttribute(Context context, AttributeSet attrs){
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.HeaderMenu);
		
		textColor = a.getColor(R.styleable.HeaderMenu_textColor, 0xFF000000);
		textFocusColor = a.getColor(R.styleable.HeaderMenu_textFocusColor, 0xFF000000);
		drawableSelected = a.getResourceId(R.styleable.HeaderMenu_selectedFrameDrawable, 0);
		drawableSplit = a.getResourceId(R.styleable.HeaderMenu_splitDrawable, 0);
		splitWidth = (int)a.getDimension(R.styleable.HeaderMenu_splitWidth, 0);
		textDisableColor = 0xFF04131A;
		a.recycle();
	}
	
	/*添加菜单*/
	public void addItem(ToolbarItem item){
		addItem(item, 1.0f);
	}
	
	/*添加菜单*/
	public void addItem(ToolbarItem item, float weight){
		//添加分隔栏
		if (splitWidth > 0 && items.size() > 0) {
			addSpliteItem();
		}
		if (item instanceof ToolbarAItem) {
			((ToolbarAItem)item).setTextColor(textColor, textFocusColor);
		}
		//ToolbarItem item = new ToolbarAItem(this, image, focus_image, text, weight);
		items.add(item);
		this.addView(item.getView());
	}
	
	/*添加空白*/
	public void addEmptyItem(float weight) {
		TextView view = new TextView(getContext());
		if (weight > 0) {
			view.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT, weight));
		} else {
			view.setLayoutParams(new LayoutParams(getItemWidth(), LayoutParams.FILL_PARENT, 0.0f));
		}
		view.setFocusable(false);
		this.addView(view);
	}
	
	/*添加SpliteItem*/
	private void addSpliteItem() {
		TextView view = new TextView(getContext());
		view.setBackgroundResource(drawableSplit);
		view.setLayoutParams(new LayoutParams(splitWidth, LayoutParams.FILL_PARENT));
		this.addView(view);
	}
	
	/*获取Item的Index*/
	public int getIndexOf(ToolbarItem item){
		return items.indexOf(item);
	}	

	/*获取每个工具项目*/
	public ToolbarItem getItem(int index){
		return items.get(index);
	}
	
	/*获取每个工具项目的宽度*/
	public int getItemWidth(){
		return itemWidth;
	}

	/*获取选中Index*/
	public int getPosition(){
		return position;
	}
	
	/*获取选中背景图片*/
	public int getSelectBGDrawableId(){
		return drawableSelected;
	}
	
	/*获取非选中背景图片*/
	public int getDefaultBGDrawableId(){
		return android.R.color.transparent;
	}
	
	/*获取不可用文字颜色*/
	public int getTextDisableColor(){
		return textDisableColor;
	}
	
	/*设置焦点*/
	public void setPosition(int value){
		Logger.v(LOG_TAG, "set Position(): value " + value);
		if (value >= 0 && value < items.size()) {
			//旧选中
			ToolbarItem item = items.get(position);
			if (item instanceof ToolbarClickableItem) {
				ToolbarClickableItem clickItem = (ToolbarClickableItem)item;
				View view = clickItem.getView();
				view.setSelected(false);
				clickItem.unFocus();
			}
			//设置选中
			position = value;
			//新选中
			item = items.get(position);
			if (item instanceof ToolbarClickableItem) {
				ToolbarClickableItem clickItem = (ToolbarClickableItem)item;
				View view = clickItem.getView();
				view.setSelected(true);
				clickItem.onFocus();
			}
		}
	}
	
	/*设置每个工具项目的宽度*/
	public void setItemWidth(int width){
		itemWidth = width;
	}
	
	/*事件侦听*/
	public void setOnItemClickListener(com.example.mylineartoolbar.customerview.OnTouchListener event){
		listener = event;
	}
	
	/*菜单可用与否设置*/
	public void setItemEnable(int index, boolean enabled){
		if (index > -1) {
			items.get(index).setEnabled(enabled);
		}
	}
	
	/*触发点击事件*/
	public void performItemClick(ToolbarItem item){
		Logger.v(LOG_TAG, "performItemClick()");
		int index = items.indexOf(item);
		if (index != -1) {
			setPosition(index);
			if (listener != null) {
				listener.onItemTouched(index, item.toString());
			}
			invalidate();
		}
	}
	
	public void performItemClick(int index){
		if (index != -1) {
			setPosition(index);
			if (listener != null) {
		    listener.onItemTouched(index, items.get(index).toString());
		    }
			invalidate();
			}
		}
	
}
